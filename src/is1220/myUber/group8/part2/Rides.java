package is1220.myUber.group8.part2;

import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.cost.CostFactory;
import is1220.myUber.group8.part2.enums.TrafficType;
import is1220.myUber.group8.part2.enums.RideStatus;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Customer;
import is1220.myUber.group8.part2.humans.Driver;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;

public class Rides {


    private ArrayList<Customer> customers;
    private Driver driver;
    private GPSCoordinate startingPoint;
    private GPSCoordinate destinationPoint;
    private ArrayList<GPSCoordinate> points;
    private Car car;
    private RideStatus status;
    private RideType type;
    private TrafficType trafficType;
    private int driverMark;
    private ZonedDateTime dateTime;

    public Rides(ArrayList<Customer> customers) {
        this.customers = customers;
        this.status = RideStatus.unconfirmed;
        this.points = new ArrayList<GPSCoordinate>();
    }

    public Rides(ArrayList<Customer> customers, GPSCoordinate startingPoint, GPSCoordinate destinationPoint) {
        this(customers);
        this.startingPoint = startingPoint;
        this.destinationPoint = destinationPoint;
    }

    public HashMap<RideType, Double> getPrices() {
        AbstractFactory cost = new CostFactory();
        HashMap<RideType, Double> costMap = new HashMap<RideType, Double>();

        for (RideType type : RideType.values()) {
            double cout = cost.getCost(type).getPrice(startingPoint.distance(destinationPoint), TrafficType.LOW_TRAFFIC);
            costMap.put(type, cout);
        }
        return costMap;
    }

    public synchronized void acceptRide(Driver driver, TrafficType trafficType, boolean accepted, BookOfRides myBook) {
        if (accepted) {
            this.status = RideStatus.confirmed;
            this.setDriver(driver);
            this.setTrafficType(trafficType);
            this.setDateTime(ZonedDateTime.now());
            //add a corresponding entry to the book of rides
            myBook.addRideToBook(this);
        } else {
            this.status = RideStatus.canceled;
        }
        this.notifyAll();
    }

    public ArrayList<Customer> getCustomers(){
    	return this.customers;
    }
    
    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }


    public GPSCoordinate getStartingPoint() {
        return startingPoint;
    }

    public void setStartingPoint(GPSCoordinate startingPoint) {
        this.startingPoint = startingPoint;
    }

    public GPSCoordinate getDestinationPoint() {
        return destinationPoint;
    }

    public void setDestinationPoint(GPSCoordinate destinationPoint) {
        this.destinationPoint = destinationPoint;
    }

    public void addPoints(GPSCoordinate point) { this.points.add(point); }

    public  ArrayList<GPSCoordinate> getPoints() { return this.points; }

    public void setPoints(ArrayList<GPSCoordinate> points) {
        this.points = points;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public RideStatus getStatus() {
        return status;
    }

    public void setStatus(RideStatus status) {
        this.status = status;
    }

    public RideType getType() {
        return type;
    }

    public void setType(RideType type) {
        this.type = type;
    }

    public int getDriverMark() {
        return driverMark;
    }

    public void setDriverMark(int driverMark) {
        if (driverMark > 0 && driverMark <= 5) {
            this.driverMark = driverMark;
        }
    }

	public TrafficType getTrafficType() {
		return trafficType;
	}

	public void setTrafficType(TrafficType trafficType) {
		this.trafficType = trafficType;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public ZonedDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(ZonedDateTime dateTime) {
		this.dateTime = dateTime;
	}
	
	
}
