package is1220.myUber.group8.part2.cars;
import is1220.myUber.group8.part2.BookOfRides;
import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.Rides;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.enums.TrafficType;
import is1220.myUber.group8.part2.humans.Driver;

import java.util.*;

/**
 * <h1> Car </h1>
 * <p> This abstract class sets the information needed for every car 
 * that could be used by myUber </p>
 * @author Benoît Sepe and Cécile Boniteau
 * @version 1.0
 */
public abstract class Car {
    private List<Driver> drivers;
    private Driver currentDriver;
    private GPSCoordinate GPSCoordinates;
    private Rides onGoingRide;
    
    public Car () {}

    /**
     * This is the constructor for the abstract class.
     * @param drivers This is the list of possible drivers of the car
     * @param currentDriver This is the driver that is currently using the car
     * @param GPSCoordinates This is the position of the car, in GPS coordinate format
     */
    public Car(List<Driver> drivers, Driver currentDriver, GPSCoordinate GPSCoordinates) {
    	this.drivers = drivers;
        this.currentDriver = currentDriver;
        this.GPSCoordinates = GPSCoordinates;
        this.onGoingRide = null;
        currentDriver.setCarUsing(this);
    }

    public void acceptRide(TrafficType trafficType, boolean accepted, BookOfRides bookOfRides) {
        onGoingRide.acceptRide(currentDriver, trafficType, accepted, bookOfRides);
    }
    
    /**
	 * This method adds a given driver to the list of possible drivers
	 * @param driver The driver that one's wishing to add to the list
	 */
    public void addDriver(Driver driver) {
        this.drivers.add(driver);
    }
    
    /**
     * This method removes a driver from the list of drivers
     * @param driver The driver to be removed
     */
    public void removeDriver(Driver driver) {
        this.drivers.remove(driver);
    }
    
    /**
     * Getter for the list of possible drivers
     * @return drivers This is the list of possible drivers
     */
    public List<Driver> getDrivers() {
        return this.drivers;
    }
    
    /**
     * Setter for the current driver. The setting is only made possible if the intended driver is in the list of possible drivers.
     * @param driver This is the driver that is currently using the car
     * @return true if success, false if not
     */
    public Boolean setCurrentDriver(Driver driver) {
        if (this.drivers.contains((driver))) {
            this.currentDriver = driver;
            driver.setCarUsing(this);
            return true;
        }
        return false;
    }
    
    /**
     * Getter for the current driver
     * @return currentDriver The driver that is presently using the car
     */
    public Driver getCurrentDriver() { return currentDriver; }


    /**
     * Setter for the current Ride
     * @param ride
     */
    public void setRide(Rides ride) {
        this.onGoingRide = ride;
    }
    /**
     * Getter for the list of on going rides
     */
    public Rides getRide() {
        return this.onGoingRide;
    }
    
    /**
     * Getter for the current position of the car
     * @return These are the GPS coordinates of the car
     */
    public GPSCoordinate getGPSCoordinates() {
        return GPSCoordinates;
    }
    
    /**
     * Setter for the current position of the car
     * @param gPSCoordinates
     */
    public void setGPSCoordinates(GPSCoordinate gPSCoordinates) {
		GPSCoordinates = gPSCoordinates;
	}

	/**
     * Getter for the maximum number of passengers (which depends of the type of car).
     * @return maxPassengers this is an integer giving the maximum number of passengers
     */
    public abstract int getMaxPassengers();
    
    /**
     * Getter for the ID of the car, following the nomenclature rule "typeOfCar + integer".
     * @return ID This is a String representing the ID of a given Car
     */
    public abstract String getID();
    
    /**
     * Getter for the types of the rides 
     */
    public abstract List<RideType> getType();

    @Override
    public String toString() {
    	String result = "";
    	for (Driver driver : this.drivers) {
    		result = result.concat(driver.getName() + " " + driver.getSurname() + ", ");
    	}
    	return result;
    }

}
