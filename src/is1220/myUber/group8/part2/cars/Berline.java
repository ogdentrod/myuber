package is1220.myUber.group8.part2.cars;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Driver;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1> Berline car</h1>
 * <p> This class extends the abstract class Car, representing the Berline type </p>
 * @author Cécile Boniteau et Benoït Sepe
 * @version 1.0
 */
public class Berline extends Car {

    private final int maxPassengers = 4;
    private String ID;

    static int compteurID = 1;
    
    public Berline () {}
    /**
     * Constructor. Also updates the counter for the ID name.
     * 
     * @param drivers @see Car
     * @param currentDriver @see Car
     * @param GPSCoordinates @see Car
     */
    public Berline(List<Driver> drivers, Driver currentDriver, GPSCoordinate GPSCoordinates) {
        super(drivers, currentDriver, GPSCoordinates);
        this.ID = "Berline" + String.valueOf(compteurID);
        compteurID++;
    }
    
    /**
     * Getter for the maxPassengers of this type of car.
     * @return 4
     */
    @Override
    public int getMaxPassengers() {
        return this.maxPassengers;
    }
    
    /**
     * Getter for the ID of this Berline, that was given to it at its construction.
     * @return ID this is a string in the format "Berline" + numberOfBerline
     */
    @Override
    public String getID() {
        return this.ID;
    }

    @Override
    public List<RideType> getType() {
        List<RideType> rides = new ArrayList<RideType>();
        rides.add(RideType.uberBlack);
        return rides;
    }

}
