/**
 * 
 */
package is1220.myUber.group8.part2.cars;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.enums.DriverState;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Driver;

/**
 * @author Cécile Boniteau et Benoît Sepe
 *
 */
class BerlineTest extends Berline {

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Berline#getMaxPassengers()}.
	 */
	
	@Test
	void testGetMaxPassengers() {
        Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Berline berline = new Berline(drivers1, driver1, position);
		//TEST
		assertEquals(berline.getMaxPassengers(), 4);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Berline#getID()}.
	 * Tests if the ID of the Berline is lesser than the current max ID, and that the string before it equals to "Berline"
	 */
	@Test
	void testGetID() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Berline berline = new Berline(drivers1, driver1, position);
		//TEST
		String IDnumber = berline.getID().replaceAll("[^0-9]", "");
		String IDtype = berline.getID().replaceAll("[^A-Za-z]+", "");
		assertTrue(Integer.parseInt(IDnumber) <= compteurID);
		assertEquals(IDtype, "Berline");
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Berline#getType()}.
	 */
	@Test
	void testGetType() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Berline berline = new Berline(drivers1, driver1, position);
		//TEST
		List<RideType> rideType = berline.getType();
		assertEquals(rideType.get(0),RideType.uberBlack );
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Car#addDriver(is1220.myUber.group8.part2.humans.Driver)}.
	 */
	@Test
	void testAddDriver() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Dracula", "Count", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Berline berline = new Berline(drivers1, driver1, position);
		//TEST
		berline.addDriver(driver2);
		assertEquals(berline.getDrivers().size(), 2);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Car#removeDriver(is1220.myUber.group8.part2.humans.Driver)}.
	 */
	@Test
	void testRemoveDriver() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Dracula", "Count", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Berline berline = new Berline(drivers1, driver1, position);
		//TEST
		berline.addDriver(driver2);
		berline.removeDriver(driver1);
		assertEquals(berline.getDrivers().size(), 1);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Car#setCurrentDriver(is1220.myUber.group8.part2.humans.Driver)}.
	 */
	@Test
	void testSetCurrentDriver() {
		Driver driver1 = new Driver("The Reaper", "John", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Dracula", "Count", DriverState.ON_DUTY);
		Driver driver3 = new Driver("Bleue", "Barbe", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Berline berline = new Berline(drivers1, driver1, position);
		//TEST
		berline.addDriver(driver2);
		berline.setCurrentDriver(driver2);
		//test setting
		assertEquals(berline.getCurrentDriver(), driver2);
		//test not setting a driver not in the list of the authorized
		berline.setCurrentDriver(driver3);
		assertEquals(berline.getCurrentDriver(), driver2);
	}


	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Car#getGPSCoordinates()}.
	 */
	@Test
	void testGetGPSCoordinates() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(0, 2.163283299999989, 158);
		Berline berline = new Berline(drivers1, driver1, position);
		//TEST
		assertEquals(berline.getGPSCoordinates(), position);
		
	}

}
