package is1220.myUber.group8.part2.cars;

import is1220.myUber.group8.part2.AbstractFactory;
import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.humans.Driver;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.cost.Cost;

import java.util.List;

/**
 * This is the class used as a Factory in the factory pattern for the cars.
 * @author Cécile Boniteau et Benoit Sepe
 * @version 1.0
 */
public class CarFactory extends AbstractFactory {
	
	/**
	 * Implementation of a factory for cars.
	 * Depending on the kind of ride, a different car is returned.
	 * @see AbstractFactory
	 */
    @Override
    public Car getCar(RideType rideType, List<Driver> drivers, Driver currentDriver, GPSCoordinate GPSCoordinates) {
        if (rideType == RideType.uberX || rideType == RideType.uberPool) {
            return new Standard(drivers, currentDriver, GPSCoordinates);
        } else if (rideType == RideType.uberBlack) {
            return new Berline(drivers, currentDriver, GPSCoordinates);
        } else if (rideType == RideType.uberVan) {
            return new Van(drivers, currentDriver, GPSCoordinates);
        }
        return null;
    }
    
    /**
	 * @see AbstractFactory
	 */
    @Override
    public Cost getCost(RideType rideType) {
        return null;
    }
}
