package is1220.myUber.group8.part2.cars;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.enums.DriverState;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Driver;

class CarFactoryTest extends CarFactory {

	@Test
	final void testGetCar() {
		CarFactory factory = new CarFactory();
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
        //Returns of GetCar
		Car supposedVan = factory.getCar(RideType.uberVan, drivers1, driver1, position);
		Car supposedStandardPool = factory.getCar(RideType.uberPool, drivers1, driver1, position);
		Car supposedBerline = factory.getCar(RideType.uberBlack, drivers1, driver1, position);
		Car supposedStandardX = factory.getCar(RideType.uberX, drivers1, driver1, position);
		//Real car types
		Berline berline = new Berline(drivers1, driver1, position);
		Standard standard = new Standard(drivers1, driver1, position);
		Van van = new Van(drivers1, driver1, position);
		//TEST
		assertEquals(supposedVan.getClass(), van.getClass());
		assertEquals(supposedBerline.getClass(), berline.getClass());
		assertEquals(supposedStandardPool.getClass(), standard.getClass());
		assertEquals(supposedStandardX.getClass(), standard.getClass());
	}

}
