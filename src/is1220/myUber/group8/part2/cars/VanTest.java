package is1220.myUber.group8.part2.cars;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.enums.DriverState;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Driver;

class VanTest extends Van {

	@Test
	void testGetMaxPassengers() {
        Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Van Van = new Van(drivers1, driver1, position);
		//TEST
		assertEquals(Van.getMaxPassengers(), 6);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Van#getID()}.
	 * Tests if the ID of the Van is lesser than the current max ID, and that the string before it equals to "Van"
	 */
	@Test
	void testGetID() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Van Van = new Van(drivers1, driver1, position);
		//TEST
		String IDnumber = Van.getID().replaceAll("[^0-9]", "");
		String IDtype = Van.getID().replaceAll("[^A-Za-z]+", "");
		assertTrue(Integer.parseInt(IDnumber) <= compteurID);
		assertEquals(IDtype, "Van");
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Van#getType()}.
	 */
	@Test
	void testGetType() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Van Van = new Van(drivers1, driver1, position);
		//TEST
		List<RideType> rideType = Van.getType();
		assertEquals(rideType.get(0),RideType.uberVan );
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Car#addDriver(is1220.myUber.group8.part2.humans.Driver)}.
	 */
	@Test
	void testAddDriver() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Dracula", "Count", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Van Van = new Van(drivers1, driver1, position);
		//TEST
		Van.addDriver(driver2);
		assertEquals(Van.getDrivers().size(), 2);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Car#removeDriver(is1220.myUber.group8.part2.humans.Driver)}.
	 */
	@Test
	void testRemoveDriver() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Dracula", "Count", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Van Van = new Van(drivers1, driver1, position);
		//TEST
		Van.addDriver(driver2);
		Van.removeDriver(driver1);
		assertEquals(Van.getDrivers().size(), 1);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Car#setCurrentDriver(is1220.myUber.group8.part2.humans.Driver)}.
	 */
	@Test
	void testSetCurrentDriver() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Dracula", "Count", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Van Van = new Van(drivers1, driver1, position);
		//TEST
		Van.addDriver(driver2);
		Van.setCurrentDriver(driver2);
		assertEquals(Van.getCurrentDriver(), driver2);
	}


	/**
	 * Test method for {@link is1220.myUber.group8.part2.cars.Car#getGPSCoordinates()}.
	 */
	@Test
	void testGetGPSCoordinates() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        GPSCoordinate position = new GPSCoordinate(0, 2.163283299999989, 158);
		Van Van = new Van(drivers1, driver1, position);
		//TEST
		assertEquals(Van.getGPSCoordinates(), position);
		
	}

}
