package is1220.myUber.group8.part2.cars;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Driver;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1> Standard car </h1>
 * <p> This class extends the abstract class Car, representing the Standard type </p>
 * @author Cécile Boniteau et Benoït Sepe
 * @version 1.0
 */
public class Standard extends Car {

    private int maxPassengers = 4;
    private String ID;

    static int compteurID = 1;
    
    public Standard () {}
    /**
     * Constructor. Also updates the counter for the ID name.
     * 
     * @param drivers @see Car
     * @param currentDriver @see Car
     * @param GPSCoordinates @see Car
     */
    public Standard(List<Driver> drivers, Driver currentDriver, GPSCoordinate GPSCoordinates) {
        super(drivers, currentDriver, GPSCoordinates);
        this.ID = "Standard" + String.valueOf(compteurID);
        compteurID++;
    }
    
    /**
     * Getter for the maxPassengers of this type of car.
     * @return 4
     */
    @Override
    public int getMaxPassengers() {
        return maxPassengers;
    }
    
    /**
     * Getter for the ID of this Standard, that was given to it at its construction.
     * @return ID this is a string in the format "Standard" + numberOfBerline
     */
    @Override
    public String getID() {
        return ID;
    }

    @Override
    public List<RideType> getType() {
        List<RideType> rides = new ArrayList<RideType>();
        rides.add(RideType.uberX);
        rides.add(RideType.uberPool);
        return rides;
    }

}
