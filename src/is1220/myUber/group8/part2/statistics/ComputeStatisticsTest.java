package is1220.myUber.group8.part2.statistics;

import static org.junit.jupiter.api.Assertions.*;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.BookOfRides;
import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.Message;
import is1220.myUber.group8.part2.Rides;
import is1220.myUber.group8.part2.myUber;
import is1220.myUber.group8.part2.algorithms.findCar.FindCarRunnable;
import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.cars.Van;
import is1220.myUber.group8.part2.enums.DriverState;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.enums.TrafficType;
import is1220.myUber.group8.part2.humans.Customer;
import is1220.myUber.group8.part2.humans.Driver;

class ComputeStatisticsTest extends ComputeStatistics {

	@Test
	final void testGetCustomerBalance() {
		myUber uber = new myUber();
		Customer cust1 = new Customer("Sepe1", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		ArrayList<Customer> customer1 = new ArrayList<Customer>();
		BookOfRides myBook = uber.getMyBook();
		customer1.add(cust1);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        Car myCar1 = new Van(drivers1, driver1, new GPSCoordinate(60, 2.163283299999989, 158));
        ArrayList<Car> cars = new ArrayList<Car>();
        cars.add(myCar1);
        
		//perform a ride
		Rides ride2 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52.7262433, 2.365247199999999, 84));
        ride2.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar = uber.findCar(ride2);
        ride2.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
		//computing the stats
	    ComputeStatistics stats = new ComputeStatistics();
	    CustomerStatistics customer1Stats = stats.getCustomerBalance(myBook, cust1);
	    //assertions
	    assertEquals(1, customer1Stats.getNumberOfRides());
	    assertTrue(29.7 < customer1Stats.getTimeSpent() && customer1Stats.getTimeSpent() < 29.8);
	    assertEquals(1161.686033113234, customer1Stats.getTotalPaid());
	}

	@Test
	final void testGetDriverStatistics() {
		ZonedDateTime begin = ZonedDateTime.now();
		myUber uber = new myUber();
		Customer cust1 = new Customer("Sepe1", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		ArrayList<Customer> customer1 = new ArrayList<Customer>();
		BookOfRides myBook = uber.getMyBook();
		customer1.add(cust1);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        Car myCar1 = new Van(drivers1, driver1, new GPSCoordinate(60, 2.163283299999989, 158));
        ArrayList<Car> cars = new ArrayList<Car>();
        cars.add(myCar1);
        
		//perform a ride
		Rides ride2 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52.7262433, 2.365247199999999, 84));
        ride2.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar = uber.findCar(ride2);
        ride2.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ZonedDateTime end = ZonedDateTime.now();
		//computing the stats
	    ComputeStatistics stats = new ComputeStatistics();
	    DriverStatistics driver1Stats = stats.getDriverStatistics(myBook, driver1, begin, end);
	    //assertions
	    assertEquals(1, driver1Stats.getNumberOfRides());
	    assertTrue(29.7 < driver1Stats.getTimeRiding() && driver1Stats.getTimeRiding() < 29.8);
	    assertEquals(1161.686033113234, driver1Stats.getTotalCashed());
	}

	@Test
	final void testGetSystemBalance() {
		ZonedDateTime begin = ZonedDateTime.now();
		myUber uber = new myUber();
		Customer cust1 = new Customer("Sepe1", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		ArrayList<Customer> customer1 = new ArrayList<Customer>();
		BookOfRides myBook = uber.getMyBook();
		customer1.add(cust1);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        Car myCar1 = new Van(drivers1, driver1, new GPSCoordinate(60, 2.163283299999989, 158));
        ArrayList<Car> cars = new ArrayList<Car>();
        cars.add(myCar1);
        
		//perform a ride
		Rides ride2 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52.7262433, 2.365247199999999, 84));
        ride2.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar = uber.findCar(ride2);
        ride2.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ZonedDateTime end = ZonedDateTime.now();
		//computing the stats
        ComputeStatistics stats = new ComputeStatistics();
        SystemStatistics systemStats = stats.getSystemBalance(myBook);
        //assertions
        assertEquals(1161.686033113234, systemStats.getTotalAmountCashedByAllDrivers());
        assertEquals(1, systemStats.getTotalNbOfRidesForAllDrivers());
	}

	@Test
	final void testGetDriversSortedByNbOfRides() {
		myUber uber = new myUber();
		Customer cust1 = new Customer("Sepe1", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Smith2", "John2", DriverState.ON_DUTY);
		ArrayList<Customer> customer1 = new ArrayList<Customer>();
		BookOfRides myBook = uber.getMyBook();
		customer1.add(cust1);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        ArrayList<Driver> drivers2 = new ArrayList<Driver>();
        drivers1.add(driver1);
        drivers2.add(driver2);
        Car myCar1 = new Van(drivers1, driver1, new GPSCoordinate(60, 2.163283299999989, 158));
        Car myCar2 = new Van(drivers2, driver2, new GPSCoordinate(60, 2, 158));
        ArrayList<Car> cars = new ArrayList<Car>();
        cars.add(myCar1);
        cars.add(myCar2);
        uber.setCars(cars);
		//perform a ride
		Rides ride2 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52.7262433, 2.365247199999999, 84));
        ride2.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar = uber.findCar(ride2);
        ride2.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //perform a second ride
        Rides ride3 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride3.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar3 = uber.findCar(ride3);
        ride3.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
      //perform a second ride
        Rides ride1 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride1.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar1 = uber.findCar(ride1);
        ride1.acceptRide(driver2, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //computing the stats
        ComputeStatistics stats = new ComputeStatistics();
        SystemStatistics systemStats = stats.getSystemBalance(myBook);
        List<Entry<Driver, Integer>> rankedDriversNbRides = stats.getDriversSortedByNbOfRides(myBook);
        //assertions
        assertEquals(driver1, rankedDriversNbRides.get(rankedDriversNbRides.size()-1).getKey());
	}

	@Test
	final void testGetDriversSortedByAppreciation() {
		myUber uber = new myUber();
		Customer cust1 = new Customer("Sepe1", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Smith2", "John2", DriverState.ON_DUTY);
		ArrayList<Customer> customer1 = new ArrayList<Customer>();
		BookOfRides myBook = uber.getMyBook();
		customer1.add(cust1);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        ArrayList<Driver> drivers2 = new ArrayList<Driver>();
        drivers1.add(driver1);
        drivers2.add(driver2);
        Car myCar1 = new Van(drivers1, driver1, new GPSCoordinate(60, 2.163283299999989, 158));
        Car myCar2 = new Van(drivers2, driver2, new GPSCoordinate(60, 2, 158));
        ArrayList<Car> cars = new ArrayList<Car>();
        cars.add(myCar1);
        cars.add(myCar2);
        uber.setCars(cars);
		//perform a ride
		Rides ride2 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52.7262433, 2.365247199999999, 84));
        ride2.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar = uber.findCar(ride2);
        ride2.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //perform a second ride
        Rides ride3 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride3.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar3 = uber.findCar(ride3);
        ride3.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
      //perform a second ride
        Rides ride1 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride1.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar1 = uber.findCar(ride1);
        ride1.acceptRide(driver2, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //
        findCar.getRide().setDriverMark(5);
        //computing the stats
        ComputeStatistics stats = new ComputeStatistics();
        SystemStatistics systemStats = stats.getSystemBalance(myBook);
        List<Entry<Driver, Integer>> rankedDriversAppreciation = stats.getDriversSortedByAppreciation(myBook);
        //assertions
        assertEquals(driver1, rankedDriversAppreciation.get(rankedDriversAppreciation.size()-1).getKey());
	}

	@Test
	final void testGetDriversSortedByAmount() {
		myUber uber = new myUber();
		Customer cust1 = new Customer("Sepe1", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Smith2", "John2", DriverState.ON_DUTY);
		ArrayList<Customer> customer1 = new ArrayList<Customer>();
		BookOfRides myBook = uber.getMyBook();
		customer1.add(cust1);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        ArrayList<Driver> drivers2 = new ArrayList<Driver>();
        drivers1.add(driver1);
        drivers2.add(driver2);
        Car myCar1 = new Van(drivers1, driver1, new GPSCoordinate(60, 2.163283299999989, 158));
        Car myCar2 = new Van(drivers2, driver2, new GPSCoordinate(60, 2, 158));
        ArrayList<Car> cars = new ArrayList<Car>();
        cars.add(myCar1);
        cars.add(myCar2);
        uber.setCars(cars);
		//perform a ride
		Rides ride2 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52.7262433, 2.365247199999999, 84));
        ride2.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar = uber.findCar(ride2);
        ride2.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //perform a second ride
        Rides ride3 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride3.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar3 = uber.findCar(ride3);
        ride3.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
      //perform a second ride
        Rides ride1 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride1.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar1 = uber.findCar(ride1);
        ride1.acceptRide(driver2, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //
        findCar.getRide().setDriverMark(5);
        //computing the stats
        ComputeStatistics stats = new ComputeStatistics();
        SystemStatistics systemStats = stats.getSystemBalance(myBook);
        List<Entry<Driver, Double>> rankedDriversAmount = stats.getDriversSortedByAmount(myBook);
        //assertions
        assertEquals(driver1, rankedDriversAmount.get(rankedDriversAmount.size()-1).getKey());
	}

	@Test
	final void testGetCustomersSortedByNbOfRides() {
		myUber uber = new myUber();
		Customer cust1 = new Customer("Sepe1", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		Customer cust2 = new Customer("Sepe2", "Benoît", new GPSCoordinate(48.0841, 2.183299999989, 150), "4944184419762889", new ArrayList<Message>());
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Smith2", "John2", DriverState.ON_DUTY);
		ArrayList<Customer> customer1 = new ArrayList<Customer>();
		ArrayList<Customer> customer2 = new ArrayList<Customer>();
		BookOfRides myBook = uber.getMyBook();
		customer1.add(cust1);
		customer2.add(cust2);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        ArrayList<Driver> drivers2 = new ArrayList<Driver>();
        drivers1.add(driver1);
        drivers2.add(driver2);
        Car myCar1 = new Van(drivers1, driver1, new GPSCoordinate(60, 2.163283299999989, 158));
        Car myCar2 = new Van(drivers2, driver2, new GPSCoordinate(60, 2, 158));
        ArrayList<Car> cars = new ArrayList<Car>();
        cars.add(myCar1);
        cars.add(myCar2);
        uber.setCars(cars);
		//perform a ride
		Rides ride2 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52.7262433, 2.365247199999999, 84));
        ride2.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar = uber.findCar(ride2);
        ride2.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //perform a second ride
        Rides ride3 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride3.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar3 = uber.findCar(ride3);
        ride3.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
      //perform a second ride
        Rides ride1 = new Rides(customer2, cust2.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride1.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar1 = uber.findCar(ride1);
        ride1.acceptRide(driver2, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //
        findCar.getRide().setDriverMark(5);
        //computing the stats
        ComputeStatistics stats = new ComputeStatistics();
        SystemStatistics systemStats = stats.getSystemBalance(myBook);
        List<Entry<Customer, Integer>> rankedCustomersNbRides = stats.getCustomersSortedByNbOfRides(myBook);
        //assertions
        assertEquals(cust1, rankedCustomersNbRides.get(rankedCustomersNbRides.size()-1).getKey());
	}

	@Test
	final void testGetCustomersSortedByAmount() {
		myUber uber = new myUber();
		Customer cust1 = new Customer("Sepe1", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		Customer cust2 = new Customer("Sepe2", "Benoît", new GPSCoordinate(48.0841, 2.183299999989, 150), "4944184419762889", new ArrayList<Message>());
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		Driver driver2 = new Driver("Smith2", "John2", DriverState.ON_DUTY);
		ArrayList<Customer> customer1 = new ArrayList<Customer>();
		ArrayList<Customer> customer2 = new ArrayList<Customer>();
		BookOfRides myBook = uber.getMyBook();
		customer1.add(cust1);
		customer2.add(cust2);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        ArrayList<Driver> drivers2 = new ArrayList<Driver>();
        drivers1.add(driver1);
        drivers2.add(driver2);
        Car myCar1 = new Van(drivers1, driver1, new GPSCoordinate(60, 2.163283299999989, 158));
        Car myCar2 = new Van(drivers2, driver2, new GPSCoordinate(60, 2, 158));
        ArrayList<Car> cars = new ArrayList<Car>();
        cars.add(myCar1);
        cars.add(myCar2);
        uber.setCars(cars);
		//perform a ride
		Rides ride2 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52.7262433, 2.365247199999999, 84));
        ride2.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar = uber.findCar(ride2);
        ride2.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //perform a second ride
        Rides ride3 = new Rides(customer1, cust1.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride3.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar3 = uber.findCar(ride3);
        ride3.acceptRide(driver1, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
      //perform a second ride
        Rides ride1 = new Rides(customer2, cust2.getGPSCoordinates(), new GPSCoordinate(52, 2, 84));
        ride1.setType(RideType.uberVan);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        FindCarRunnable findCar1 = uber.findCar(ride1);
        ride1.acceptRide(driver2, TrafficType.LOW_TRAFFIC, true, myBook);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //
        findCar.getRide().setDriverMark(5);
        //computing the stats
        ComputeStatistics stats = new ComputeStatistics();
        SystemStatistics systemStats = stats.getSystemBalance(myBook);
        List<Entry<Customer, Double>> rankedCustomersAppreciation = stats.getCustomersSortedByAmount(myBook);
        //assertions
        assertEquals(cust1, rankedCustomersAppreciation.get(rankedCustomersAppreciation.size()-1).getKey());
	}

}
