/**
 * 
 */
package is1220.myUber.group8.part2.statistics;

/**
 * @author Cécile Boniteau and Benoît Sepe
 *
 */
public class CustomerStatistics {
	private int numberOfRides;
	private double timeSpent;
	private double totalPaid;
	
	public CustomerStatistics() {
	}
	
	public int getNumberOfRides() {
		return numberOfRides;
	}
	public void setNumberOfRides(int numberOfRides) {
		this.numberOfRides = numberOfRides;
	}
	public double getTimeSpent() {
		return timeSpent;
	}
	public void setTimeSpent(double timeSpent) {
		this.timeSpent = timeSpent;
	}
	public double getTotalPaid() {
		return totalPaid;
	}
	public void setTotalPaid(double totalPaid) {
		this.totalPaid = totalPaid;
	}
	
}
