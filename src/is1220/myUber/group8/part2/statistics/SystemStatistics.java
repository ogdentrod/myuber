/**
 * 
 */
package is1220.myUber.group8.part2.statistics;

import java.util.HashMap;

import is1220.myUber.group8.part2.BookEntry;
import is1220.myUber.group8.part2.BookOfRides;
import is1220.myUber.group8.part2.Rides;
import is1220.myUber.group8.part2.cost.CostFactory;
import is1220.myUber.group8.part2.humans.Customer;
import is1220.myUber.group8.part2.humans.Driver;

/**
 * @author Benoit Sepe et Cécile Boniteau
 *
 */
public class SystemStatistics {
	
	private CostFactory factory = new CostFactory();
	
	private HashMap<Driver, Integer> nbRidesToDriverDict = new HashMap<Driver, Integer>();
	private HashMap<Driver, Double> totalAmountToDriverDict = new HashMap<Driver, Double>();
	private HashMap<Driver, Integer> appreciationToDriverDict = new HashMap<Driver, Integer>();
	private HashMap<Customer, Integer> nbRidesToCustomerDict = new HashMap<Customer, Integer>();
	private HashMap<Customer, Double> totalAmountToCustomerDict = new HashMap<Customer, Double>();
	
	private int totalNbOfRidesForAllDrivers = 0;
	private double totalAmountCashedByAllDrivers = 0;
	
	public SystemStatistics(BookOfRides book) {
		for (BookEntry entry : book.getBook()) {
			Rides ride = entry.getRide();
			//Drivers information
			Driver driver = ride.getDriver();
			//Test whether the driver is already in the relevant dictionary
			if (nbRidesToDriverDict.containsKey(driver)) {
				nbRidesToDriverDict.put(driver, nbRidesToDriverDict.get(driver) + 1);
				totalAmountToDriverDict.put(driver, totalAmountToDriverDict.get(driver) + factory.getCost(ride.getType()).getPrice(entry.getDistance(), ride.getTrafficType()));
				appreciationToDriverDict.put(driver, appreciationToDriverDict.get(driver) + ride.getDriverMark());
			} else {
				nbRidesToDriverDict.put(driver, 1);
				totalAmountToDriverDict.put(driver, factory.getCost(ride.getType()).getPrice(entry.getDistance(), ride.getTrafficType()));
				appreciationToDriverDict.put(driver, ride.getDriverMark());
			}
			totalAmountCashedByAllDrivers = totalAmountCashedByAllDrivers + totalAmountToDriverDict.get(driver);
			totalNbOfRidesForAllDrivers = totalNbOfRidesForAllDrivers + nbRidesToDriverDict.get(driver);
			//Customers information
			for (Customer customer : ride.getCustomers()) {
				//Key already created
				if (nbRidesToCustomerDict.containsKey(customer)) {
					nbRidesToCustomerDict.put(customer, nbRidesToCustomerDict.get(customer) + 1);
					totalAmountToCustomerDict.put(customer, totalAmountToCustomerDict.get(customer) + factory.getCost(ride.getType()).getPrice(entry.getDistance(), ride.getTrafficType()));
				} else {
					nbRidesToCustomerDict.put(customer, 1);
					totalAmountToCustomerDict.put(customer, factory.getCost(ride.getType()).getPrice(entry.getDistance(), ride.getTrafficType()));
				}
			}
		}
	}
	
	public HashMap<Driver, Integer> getNbRidesToDriversDict() {
		return nbRidesToDriverDict;
	}
	public void setNbRidesToDriversDict(HashMap<Driver, Integer> nbRidesToDriversDict) {
		this.nbRidesToDriverDict = nbRidesToDriversDict;
	}
	public HashMap<Driver, Double> getTotalAmountToDriverDict() {
		return totalAmountToDriverDict;
	}
	public void setTotalAmountToDriverDict(HashMap<Driver, Double> totalAmountToDriverDict) {
		this.totalAmountToDriverDict = totalAmountToDriverDict;
	}
	public HashMap<Driver, Integer> getAppreciationToDriverDict() {
		return appreciationToDriverDict;
	}
	public void setAppreciationToDriverDict(HashMap<Driver, Integer> appreciationToDriverDict) {
		this.appreciationToDriverDict = appreciationToDriverDict;
	}
	public HashMap<Customer, Integer> getNbRidesToCustomerDict() {
		return nbRidesToCustomerDict;
	}
	public void setNbRidesToCustomerDict(HashMap<Customer, Integer> nbRidesToCustomerDict) {
		this.nbRidesToCustomerDict = nbRidesToCustomerDict;
	}
	public HashMap<Customer, Double> getTotalAmountToCustomerDict() {
		return totalAmountToCustomerDict;
	}
	public void setTotalAmountToCustomerDict(HashMap<Customer, Double> totalAmountToCustomerDict) {
		this.totalAmountToCustomerDict = totalAmountToCustomerDict;
	}

	public HashMap<Driver, Integer> getNbRidesToDriverDict() {
		return nbRidesToDriverDict;
	}

	public void setNbRidesToDriverDict(HashMap<Driver, Integer> nbRidesToDriverDict) {
		this.nbRidesToDriverDict = nbRidesToDriverDict;
	}

	public int getTotalNbOfRidesForAllDrivers() {
		return totalNbOfRidesForAllDrivers;
	}

	public void setTotalNbOfRidesForAllDrivers(int totalNbOfRidesForAllDrivers) {
		this.totalNbOfRidesForAllDrivers = totalNbOfRidesForAllDrivers;
	}

	public double getTotalAmountCashedByAllDrivers() {
		return totalAmountCashedByAllDrivers;
	}

	public void setTotalAmountCashedByAllDrivers(int totalAmountCashedByAllDrivers) {
		this.totalAmountCashedByAllDrivers = totalAmountCashedByAllDrivers;
	}
	
	
}
