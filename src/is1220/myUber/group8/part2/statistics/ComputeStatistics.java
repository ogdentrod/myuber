/**
 * 
 */
package is1220.myUber.group8.part2.statistics;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import is1220.myUber.group8.part2.BookEntry;
import is1220.myUber.group8.part2.BookOfRides;
import is1220.myUber.group8.part2.Rides;
import is1220.myUber.group8.part2.cost.CostFactory;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Customer;
import is1220.myUber.group8.part2.humans.Driver;
import is1220.myUber.group8.part2.utils.EntryCustomerDoubleComparator;
import is1220.myUber.group8.part2.utils.EntryCustomerIntegerComparator;
import is1220.myUber.group8.part2.utils.EntryDriverDoubleComparator;
import is1220.myUber.group8.part2.utils.EntryDriverIntegerComparator;

/**
 * @author Cécile Boniteau and Benoît Sepe
 *
 */
public class ComputeStatistics {
	
	private CostFactory factory = new CostFactory();
	/**
	 * Allows to retrieve the number of rides, the total time spent on a uber car, 
	 * total paid for a given customer
	 * @param book the Book of Rides
	 * @param customer the customer we want to have statistics on
	 */
	public CustomerStatistics getCustomerBalance (BookOfRides book, Customer customer) {
		int numberOfRides = 0;
		double timeSpent = 0;
		double totalPaid = 0;
		//iterate over all entries
		for (BookEntry entry : book.getBook()) {
			//For UberPool, numerous customers can be assigned to one ride
			Rides ride = entry.getRide();
			if (ride.getType() == RideType.uberPool) {
				for (Customer entryCustomer : ride.getCustomers()) {
					if (entryCustomer.getID() == customer.getID()) {
						numberOfRides = numberOfRides + 1;
						timeSpent = timeSpent + entry.getDuration();
						totalPaid = totalPaid + factory.getCost(ride.getType()).getPrice(entry.getDistance(), ride.getTrafficType());
					}
				}
			} else {
			//For other types there is only one customer per ride
				Customer entryCustomer = ride.getCustomers().get(0);
				if (entryCustomer.getID() == customer.getID()) {
					numberOfRides = numberOfRides + 1;
					timeSpent = timeSpent + entry.getDuration();
					totalPaid = totalPaid + factory.getCost(ride.getType()).getPrice(entry.getDistance(), ride.getTrafficType());
				}
			}
		}
		//create statistics object
		CustomerStatistics customerStatistics = new CustomerStatistics();
		customerStatistics.setNumberOfRides(numberOfRides);
		customerStatistics.setTimeSpent(timeSpent);
		customerStatistics.setTotalPaid(totalPaid);
		
		return customerStatistics;
	}
	
	/**
	 * Computes the total number of rides executed in a given time interval
	 * the total amount of money cashed in
	 * on-duty rate of driving, the ratio between the time spent driving and being on duty
	 * @param book the BookOfRides
	 * @param driver the driver we want to have statistics on
	 */
	public DriverStatistics getDriverStatistics(BookOfRides book, Driver driver, ZonedDateTime begin, ZonedDateTime end) {
		int numberOfRides = 0;
		double totalCashed = 0;
		double timeRiding = 0;
		//iterate over all entries
		for (BookEntry entry : book.getBook()) {
			Rides ride = entry.getRide();
			ZonedDateTime dateEntry = ride.getDateTime();
			//check if in time interval
			if (begin.isBefore(dateEntry) && end.isAfter(dateEntry)) {
				Driver entryDriver = ride.getDriver();
				if (entryDriver.getID() == driver.getID()) {
					numberOfRides = numberOfRides + 1;
					timeRiding = timeRiding + entry.getDuration();
					totalCashed = totalCashed + factory.getCost(ride.getType()).getPrice(entry.getDistance(), ride.getTrafficType());
				}
			}
		}
		DriverStatistics driverStat = new DriverStatistics();
		driverStat.setBegin(begin);
		driverStat.setEnd(end);
		driverStat.setNumberOfRides(numberOfRides);
		driverStat.setTimeRiding(timeRiding);
		driverStat.setTotalCashed(totalCashed);
		
		return driverStat;
	}
	
	/**
	 * Returns an object giving the total number of rides for all drivers, total amount of money cashed in
	 * and sorted lists of drivers and customers
	 * @param book the book of rides
	 * @param drivers list of all drivers existing in myUber
	 * @param customers list of all customers existing in myUber
	 */
	public SystemStatistics getSystemBalance(BookOfRides book) {
		SystemStatistics systemStats = new SystemStatistics(book);
		return systemStats;
	}
	
	public List<Entry<Driver, Integer>>getDriversSortedByNbOfRides(BookOfRides book) {
		SystemStatistics systemStatistics = getSystemBalance(book);
		HashMap<Driver, Integer> map = systemStatistics.getNbRidesToDriverDict();
		List<Map.Entry<Driver, Integer>> driversListByNbOfRides =new LinkedList<Entry<Driver, Integer>>(map.entrySet());
		driversListByNbOfRides.sort(new EntryDriverIntegerComparator());
		return driversListByNbOfRides;
	}
	
	public List<Entry<Driver, Integer>> getDriversSortedByAppreciation(BookOfRides book) {
		SystemStatistics systemStatistics = getSystemBalance(book);
		HashMap<Driver, Integer> map = systemStatistics.getAppreciationToDriverDict();
		List<Map.Entry<Driver, Integer>> driversListByAppreciation =new LinkedList<Entry<Driver, Integer>>(map.entrySet());
		driversListByAppreciation.sort(new EntryDriverIntegerComparator());
		return driversListByAppreciation;
	}
	
	public List<Entry<Driver, Double>> getDriversSortedByAmount(BookOfRides book) {
		SystemStatistics systemStatistics = getSystemBalance(book);
		HashMap<Driver, Double> map = systemStatistics.getTotalAmountToDriverDict();
		List<Map.Entry<Driver, Double>> driversbyAmount =new LinkedList<Entry<Driver, Double>>(map.entrySet());
		driversbyAmount.sort(new EntryDriverDoubleComparator());
		return driversbyAmount;
	}
	
	public List<Entry<Customer, Integer>> getCustomersSortedByNbOfRides(BookOfRides book) {
		SystemStatistics systemStatistics = getSystemBalance(book);
		HashMap<Customer, Integer> map = systemStatistics.getNbRidesToCustomerDict();
		List<Map.Entry<Customer, Integer>> customersListbyNb =new LinkedList<Entry<Customer, Integer>>(map.entrySet());
		customersListbyNb.sort(new EntryCustomerIntegerComparator());
		return customersListbyNb;
	}
	
	public List<Entry<Customer, Double>> getCustomersSortedByAmount(BookOfRides book) {
		SystemStatistics systemStatistics = getSystemBalance(book);
		HashMap<Customer, Double> map = systemStatistics.getTotalAmountToCustomerDict();
		List<Map.Entry<Customer, Double>> customersByAmount =new LinkedList<Entry<Customer, Double>>(map.entrySet());
		customersByAmount.sort(new EntryCustomerDoubleComparator());
		return customersByAmount;
	}
	
}
