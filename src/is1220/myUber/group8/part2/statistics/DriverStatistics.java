/**
 * 
 */
package is1220.myUber.group8.part2.statistics;

import java.time.ZonedDateTime;

/**
 * @author Cécile Boniteau and Benoit Sepe
 *
 */
public class DriverStatistics {
	private int numberOfRides;
	private double totalCashed;
	private double timeRiding;
	private ZonedDateTime begin;
	private ZonedDateTime end;
	
	public DriverStatistics() {
	}
	
	public int getNumberOfRides() {
		return numberOfRides;
	}
	public void setNumberOfRides(int numberOfRides) {
		this.numberOfRides = numberOfRides;
	}
	public double getTotalCashed() {
		return totalCashed;
	}
	public void setTotalCashed(double totalCashed) {
		this.totalCashed = totalCashed;
	}

	public double getTimeRiding() {
		return timeRiding;
	}

	public void setTimeRiding(double timeRiding) {
		this.timeRiding = timeRiding;
	}

	public ZonedDateTime getBegin() {
		return begin;
	}

	public void setBegin(ZonedDateTime begin) {
		this.begin = begin;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(ZonedDateTime end) {
		this.end = end;
	}
	
	
}
