package is1220.myUber.group8.part2;

import java.util.Date;

public class Message {
    private Date date;
    private String message;

    public Message(Date date, String message) {
        this.date = date;
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }
}
