package is1220.myUber.group8.part2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Map.Entry;

import is1220.myUber.group8.part2.algorithms.findCar.FindCarRunnable;
import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.cars.CarFactory;
import is1220.myUber.group8.part2.cost.CostFactory;
import is1220.myUber.group8.part2.enums.DriverState;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.enums.TrafficType;
import is1220.myUber.group8.part2.humans.Customer;
import is1220.myUber.group8.part2.humans.Driver;
import is1220.myUber.group8.part2.statistics.ComputeStatistics;
import is1220.myUber.group8.part2.statistics.CustomerStatistics;
import is1220.myUber.group8.part2.statistics.DriverStatistics;
import is1220.myUber.group8.part2.utils.ConsoleOutputCapturer;
import is1220.myUber.group8.part2.utils.numericStringGenerator;


public class CLUI {

	private myUber myUber;
	private Scanner scanner;

    private static String[] exlude_methods = {"CLUI", "run", "runMethod"};


    public CLUI() {
        this.myUber = new myUber();
        this.scanner = new Scanner(System.in);
    }


    public void run() {
		System.out.println();
		System.out.println();
		System.out.println("WARNING : Our program uses geographic coordinates instead of 2D coordinates. See https://en.wikipedia.org/wiki/Geographic_coordinate_system");

        String input = "";

        while (input != "quit") {
            System.out.println();
            System.out.print("> ");
            input = this.scanner.nextLine();

            String[] parts = input.split(" ");

            runMethod(parts);

            System.out.println();

        }

        System.out.println();
        this.scanner.close();
        System.out.println("See you soon !");
    }


    /**
     * @param parts
     * This method verify the arguments, and run the corresponding method that need to be called
     */
    public void runMethod(String[] parts) {
        Class<?> c = this.getClass();
        Method[] allMethods = c.getDeclaredMethods();

        boolean found = false;
        for (Method m : allMethods) {
            if (!m.getName().equals(parts[0]) || Arrays.stream(CLUI.exlude_methods).anyMatch(x -> x == parts[0])) {
                continue;
            }
            found = true;

            if (m.getParameterCount() == parts.length-1) {
                try {
                    ArrayList<String> listofArgs = new ArrayList<>(Arrays.asList(parts));
                    listofArgs.remove(0);
                    m.invoke(this, listofArgs.toArray(new String[0]));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println(parts[0] + " expects " + m.getParameterCount() + " arguments, but you specified " + String.valueOf(parts.length-1));
            }
        }
        if (!found) {
            System.out.println("Unknown command");
        }
    }
    
    /**
     * Initializes a myUber system with the execution of the CLUI commands stored in the file named absolute_path/fileName.ini
     * @param fileName
     */
    public void init(String fileName) {
    	try {
    		//get all lines stored in .ini file
			List<String> allLines = Files.readAllLines(Paths.get(fileName + ".ini"));
			for (String line : allLines) {
				//split the string in parts
				String [] parts = line.split(" ");
				//run the corresponding method
				this.runMethod(parts);
				//print a new line
				System.out.println();
			}
			this.runMethod("displayState".split(" "));
		} catch (IOException e) {
			//Explain that the file name is not good or not found
			System.out.println("_____");
			System.out.println();
    		System.out.println("The value you specified for the file name, or its absolute path and name wasn't correct");
    		System.out.println("Please make sure you type it correctly, with the proper case. As an example it could be : ");
    		System.out.println("->  /home/user_name/Documents/myuber/myuber");
    		System.out.println("->  myuber");
    		System.out.println("_____");
		}
	}
    
    /**
     * Setups a new myUber by creating a specified number of each car type, and customers.
     * @param nStandardCars
     * @param nBerlineCars
     * @param nVanCars
     * @param nCustomers
     */
    public void setup(String nStandardCars, String nBerlineCars, String nVanCars, String nCustomers) {
    	//converting to integers
    	try {
    		Integer standardNb = Integer.valueOf(nStandardCars);
    		Integer berlineNb = Integer.valueOf(nBerlineCars);
    		Integer vanNb = Integer.valueOf(nVanCars);
    		Integer customerNb = Integer.valueOf(nCustomers);
	    	//re-initialization
	    	this.myUber = new myUber();
	    	//customers creation
	    	for (int i=0 ; i<customerNb ; i++) {
	    		String name_i = "customer" + i + "Name";
	    		String surname_i = "customer" + i + "Surname";
	    		String[] parts = ("addCustomer" + " " + name_i + " " + surname_i).split(" ");
	    		//run the corresponding method
				this.runMethod(parts);
				//print a new line
				System.out.println();
	    	}
	    	//standard cars creation
	    	for (int i=0 ; i<standardNb+berlineNb+vanNb ; i++) {
	    		String name_i = "driver" + i + "Name";
	    		String surname_i = "driver" + i + "Surname";
	    		String carType;
	    		//getting the type of car to be created
	    		if (i<standardNb) {
	    			carType = "standard";
	    		} else if (i < standardNb+berlineNb){
	    			carType = "berline";
	    		} else {
	    			carType = "van";
	    		}
	    		//building the command
	    		String[] parts = ("addCarDriver" + " " + name_i + " " + surname_i + " " + carType).split(" ");
	    		//run the corresponding method
				this.runMethod(parts);

				String[] parts2 = ("setDriverStatus " + name_i + " " + surname_i + " " + "onduty").split(" ");
				this.runMethod(parts2);

				//print a new line
				System.out.println();
	    	}
	    	//display system state
	    	this.runMethod("displayState".split(" "));
    	}catch(NumberFormatException e) {
			System.out.println("_____");
			System.out.println();
    		System.out.println("The value you specified for one of the four arguments wasn't right");
    		System.out.println("Please make sure you type it correctly, the four of them must be integers ");
    		System.out.println("_____");
    	}
    }
    
    /**
     * Execute the scenario contained in fileNameInput (which must be a txt file) 
     * and outputs what happens in fileNameOuput (creating it necessary, and clearing it before otherwise)
     * @param fileNameInput
     * @param fileNameOutput
     */
    public void runtest(String fileNameInput, String fileNameOutput) {
    	try {
    		//create the console output capturer and starting it
    		ConsoleOutputCapturer capturer = new ConsoleOutputCapturer();
    		capturer.start();
    		//get all lines stored in .txt file
			List<String> allLines = Files.readAllLines(Paths.get(fileNameInput + ".txt"));
			for (String line : allLines) {
				int commentIndex = line.indexOf("//");
				if (commentIndex != -1)
				{
					line = line.substring(0, commentIndex);
				}
				//split the string in parts
				String [] parts = line.split(" ");

				//run the corresponding method
				this.runMethod(parts);
				//print a new line
				System.out.println();
			}
			this.runMethod("displayState".split(" "));
			String toStoreInFile = capturer.stop();
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileNameOutput + ".txt"))) {
				bw.write(toStoreInFile);
				System.out.println("_____");
				System.out.println();
	    		System.out.println("The given scenario was executed and the output can be found in the specified output file");
	    		System.out.println("_____");
			} catch (IOException e) {
				System.out.println("_____");
				System.out.println();
	    		System.out.println("There was a problem with ouputing the result in the specified output file");
	    		System.out.println("_____");
			}
		} catch (IOException e) {
			// Explain that the file name is not good or not found
			System.out.println("_____");
			System.out.println();
    		System.out.println("The value you specified for the input file name, or its absolute path and name, wasn't correct");
    		System.out.println("Please make sure you type it correctly, with the proper case. As an example it could be : ");
    		System.out.println("->  /home/user_name/Documents/myuber/myuber");
    		System.out.println("->  myuber");
    		System.out.println("_____");
		}
	}
    
    /**
     * This method adds a customer to myUber and outputs the names of all drivers.
     * The generated customer is located at a random place in a particular area.
     * The card number is generated from the output of the to string method applied to the coordinates
     * @param name
     * @param surname
     */
    public void addCustomer(String name, String surname) {
    	Customer customer = new Customer(name, surname, GPSCoordinate.randomCoordinates(10, 20), numericStringGenerator.generateString(), new ArrayList<Message>());
    	myUber.addCustomer(customer);
    	//Printing of information in the CLUI
    	System.out.println("_____");
    	System.out.println("The new customer has been added to the list of known customers");
    	System.out.println("Please find below the names of all customers : ");
    	for (Customer cust : myUber.getCustomers()) {
    		System.out.println("-----");
    		System.out.println(cust.getName() + " " + cust.getSurname());
    		System.out.println("       | Located at " + cust.getGPSCoordinates().toString());
    		System.out.println("       | Customer ID " + cust.getID());
    	}
    	System.out.println("_____");
    }
    
    /**
     * Adds a driver and its car in the world.
     * @param name
     * @param surname
     * @param carType must respect the case of the different types of ride
     */
    public void addCarDriver(String name, String surname, String carType) {
    	// creating a factory of cars
    	CarFactory factory = new CarFactory();
    	// preparing the driver
    	Driver driver = new Driver(name, surname, DriverState.OFFLINE);
    	ArrayList<Driver> drivers = new ArrayList<Driver>();
    	drivers.add(driver);
    	//preparing the car
    	try {
    		String rideType = "";
    		switch (carType){
    			case "standard": rideType = "uberX";
    							break;
    			case "berline" : rideType = "uberBlack";
    							break;
    			case "van" : rideType = "uberVan";
    							break;
    		}
    		RideType type = RideType.valueOf(rideType);
	    	Car car = factory.getCar(type, drivers, driver, GPSCoordinate.randomCoordinates(10,20));
	    	//updating myUber
	    	myUber.addDriver(driver);
	    	myUber.addCar(car);
			driver.setCarUsing(car);
	    	//Printing of information in the CLUI
	    	System.out.println("_____");
	    	System.out.println("The new driver and its car have been added to the list of known drivers and cars");
	    	System.out.println("Please find below the names of all drivers and their cars : ");
	    	for (Car oneCar : myUber.getCars()) {
	    		System.out.println("-----");
	    		System.out.println(oneCar.getID() + " - type : " + oneCar.getType().toString());
	    		System.out.println("       | Located at " + oneCar.getGPSCoordinates().toString());
	    		System.out.println("       | Currently being driven by : " + oneCar.getCurrentDriver().getName() + " " + oneCar.getCurrentDriver().getSurname());
	    		System.out.println("       | List of its drivers : " + oneCar);
	    	}
	    	System.out.println("_____");
    	} catch (IllegalArgumentException e) {
    		//if the given car type does not exist or was mistyped
    		System.out.println("_____");
    		System.out.println("The value you gave for car type is not right");
    		System.out.println("Please make sure you type it correctly, with the proper case. As a reminder, the accepted values are : ");
    		System.out.println("-> standard");
    		System.out.println("-> van");
    		System.out.println("-> berline");
    		System.out.println("_____");
    	}
    }
    
    /**
     * Adds a driver to the car specified by its ID
     * @param name
     * @param surname
     * @param carID : must follow the notation of car IDs
     */
    public void addDriver(String name, String surname, String carID) {
    	//creating the driver
    	Driver driver = new Driver(name, surname, DriverState.OFFLINE);
    	ArrayList<Driver> drivers = new ArrayList<Driver>();
    	drivers.add(driver);
    	//finding the car with the given ID
    	Car targetCar = null;
    	for (Car existingCar : myUber.getCars()) {
    		if(existingCar.getID().equals(carID)) {
    			targetCar = existingCar;
    			break;
    		}
    	}
    	if (targetCar != null) {
	    	//if we have found the car
	    	targetCar.addDriver(driver);
	    	//we also update myUber drivers
	    	myUber.addDriver(driver);
	    	driver.setCarUsing(targetCar);
	    	//Printing of information in the CLUI
	    	System.out.println("_____");
	    	System.out.println("The new driver has been added to the list of known drivers, and was attached to the given car");
	    	System.out.println("Please find below the names of all drivers and their cars : ");
	    	for (Car oneCar : myUber.getCars()) {
	    		System.out.println("-----");
	    		System.out.println(oneCar.getID() + " - type : " + oneCar.getType().toString());
	    		System.out.println("       | Located at " + oneCar.getGPSCoordinates().toString());
	    		System.out.println("       | Currently being driven by : " + oneCar.getCurrentDriver().getName() + " " + oneCar.getCurrentDriver().getSurname());
	    		System.out.println("       | List of its drivers : " + oneCar);
	    	}
	    	System.out.println("_____");
    	} else {
    		//if the given car  ID does not exist or was mistyped
    		System.out.println("_____");
    		System.out.println("The value you gave for car ID is not right");
    		System.out.println("Please make sure you type it correctly, with the proper case. As a reminder, the accepted values are : ");
    		System.out.println("-> StandardINT");
    		System.out.println("-> VanINT");
    		System.out.println("-> BerlineINT");
    		System.out.println("With INT a placeholder for an integer");
    		System.out.println("_____");
    	}
    }
    
    /**
     * Sets the status of the driver given by the name / surname pair
     * If multiple drivers share the same name and surname, they all have their status changed
     * @param name
     * @param surname
     * @param status
     */
    public void setDriverStatus(String name, String surname, String status) {
    	//convert the status to a DriverState if possible
    	try {
    		String driverState = "";
    		//convert the given string to a properly cased one
    		switch (status){
    			case "offduty": driverState = "OFF_DUTY";
    							break;
    			case "onduty" : driverState = "ON_DUTY";
    							break;
    			case "offline" : driverState = "OFFLINE";
    							break;
    		}
    		//convert to driverState
    		DriverState state = DriverState.valueOf(driverState);
    		ArrayList<Driver> possibleDrivers = new ArrayList<Driver>();
    		//find the driver among all drivers
        	for (Driver knownDriver : myUber.getDrivers()) {
        		if (knownDriver.getName().equals(name) && knownDriver.getSurname().equals(surname)) {
        			possibleDrivers.add(knownDriver);
        			break;
        		}
        	}
        	//if we found at least one corresponding driver
        	if (possibleDrivers.size() >= 1) {
        		for (Driver driverToUpdate : possibleDrivers) {
        			driverToUpdate.setState(state);
        		}
        		//Printing of information in the CLUI
    	    	System.out.println("_____");
    	    	System.out.println("The driver's state has been updated");
    	    	System.out.println("Please find below the names of all drivers and some information about them");
    	    	for (Driver driver : myUber.getDrivers()) {
    	    		System.out.println("-----");
    	    		System.out.println(driver.getName() +" "+driver.getSurname());
    	    		System.out.println("       | Car currently being used : " + driver.getCarUsing().getID());
    	    		System.out.println("       | Current position : " + driver.getCarUsing().getGPSCoordinates());
    	    		System.out.println("       | Driver state : " + driver.getState());
    	    	}
    	    	System.out.println("_____");
        	} else {
        		//if the given driver name and surname combination does not yield any found driver to update
        		System.out.println("_____");
        		System.out.println("The system did not find any driver with the name and surname you specified");
        		System.out.println("Please make sure you type correctly, with the proper case.");
        		System.out.println("_____");
        	}
    	} catch (IllegalArgumentException e) {
    		//if the given driver status does not exist or was mistyped
    		System.out.println("_____");
    		System.out.println("The value you gave for driver status is not right");
    		System.out.println("Please make sure you type it correctly, with the proper case. As a reminder, the accepted values are : ");
    		System.out.println("-> offduty");
    		System.out.println("-> onduty");
    		System.out.println("-> offline");
    		System.out.println("_____");
    	}
    	
	}
    
    /**
     * Moves the car specified by its id to the given position
     * @param carID
     * @param lat
     * @param lon
     */
    public void moveCar(String carID, String lat, String lon) {
    	//converting the given coordinates into numbers
    	try {
	    	double convertedLat = Double.parseDouble(lat.replace(",",".") );
	    	double convertedLon = Double.parseDouble(lon.replace(",",".") );
	    	//we attribute the medium elevation of France
	    	double convertedEl = 375;
	    	//making the GPSCoordinate object
	    	GPSCoordinate position = new GPSCoordinate(convertedLat, convertedLon, convertedEl);
	    	//finding the car with the given ID
	    	Car targetCar = null;
	    	for (Car existingCar : myUber.getCars()) {
	    		if(existingCar.getID().equals(carID)) {
	    			targetCar = existingCar;
	    			break;
	    		}
	    	}
	    	if (targetCar != null) {
		    	//if we have found the car
	    		targetCar.setGPSCoordinates(position);
		    	//Printing of information in the CLUI
		    	System.out.println("_____");
		    	System.out.println("The car was moved to the new position");
		    	System.out.println("Please find below the list of all cars : ");
		    	for (Car oneCar : myUber.getCars()) {
		    		System.out.println("-----");
		    		System.out.println(oneCar.getID() + " - type : " + oneCar.getType().toString() + " - located at " + oneCar.getGPSCoordinates().toString());
		    		System.out.println("       | Currently being driven by : " + oneCar.getCurrentDriver().getName() + " " + oneCar.getCurrentDriver().getSurname());
		    		System.out.println("       | List of its drivers : " + oneCar);
		    	}
		    	System.out.println("_____");
	    	} else {
	    		//if the given car  ID does not exist or was mistyped
	    		System.out.println("_____");
	    		System.out.println("The value you gave for car ID is not right");
	    		System.out.println("Please make sure you type it correctly, with the proper case. As a reminder, the accepted values are : ");
	    		System.out.println("-> StandardINT");
	    		System.out.println("-> VanINT");
	    		System.out.println("-> BerlineINT");
	    		System.out.println("With INT a placeholder for an integer");
	    		System.out.println("_____");
	    	}
    	} catch (NumberFormatException e) {
    		//if the given driver status does not exist or was mistyped
    		System.out.println("_____");
    		System.out.println("The value you gave for latitude or longitude is not right");
    		System.out.println("Please make sure you type a number, for example : ");
    		System.out.println("-> 10");
    		System.out.println("-> 10.1");
    		System.out.println("-> 10,3");
    		System.out.println("_____");
    	}
	}
    
    /**
     * Moves the specified customer to the given position
     * @param custID
     * @param lat
     * @param lon
     */
    public void moveCustomer(String custID, String lat, String lon) {
    	//converting the given coordinates into numbers
    	try {
	    	double convertedLat = Double.parseDouble(lat.replace(",",".") );
	    	double convertedLon = Double.parseDouble(lon.replace(",",".") );
	    	//we attribute the medium elevation of France
	    	double convertedEl = 375;
	    	//making the GPSCoordinate object
	    	GPSCoordinate position = new GPSCoordinate(convertedLat, convertedLon, convertedEl);
	    	//finding the specified customer
	    	Customer targetCustomer = null;
	    	for (Customer knownCust : myUber.getCustomers()) {
	    		//converting the id of the customer being iterated on to String for comparison
	    		String IDKnownCust = String.valueOf(knownCust.getID());
	    		if(IDKnownCust.equals(custID)) {
	    			targetCustomer = knownCust;
	    			break;
	    		}
	    	}
	    	if (targetCustomer != null) {
	    		//if we have found the car
	    		targetCustomer.setGPSCoordinates(position);
		    	//Printing of information in the CLUI
		    	System.out.println("_____");
		    	System.out.println("The customer was moved to the new position");
		    	System.out.println("Please find below the names of all customers : ");
		    	for (Customer cust : myUber.getCustomers()) {
		    		System.out.println("-----");
		    		System.out.println(cust.getName() + " " + cust.getSurname() + " - located at " + cust.getGPSCoordinates().toString());
		    	}
		    	System.out.println("_____");
	    	} else {
	    		//if the given car  ID does not exist or was mistyped
	    		System.out.println("_____");
	    		System.out.println("The value you gave for customer ID is not right");
	    		System.out.println("Please make sure you type it correctly, it must be an integer");

	    		System.out.println("_____");
	    	}
    	} catch (NumberFormatException e) {
    		//if the given driver status does not exist or was mistyped
    		System.out.println("_____");
    		System.out.println("The value you gave for latitude or longitude is not right");
    		System.out.println("Please make sure you type a number, for example : ");
    		System.out.println("-> 10");
    		System.out.println("-> 10.1");
    		System.out.println("-> 10,3");
    		System.out.println("_____");
    	}
	}
    
    /**
     * Displays the state of the system
     */
    public void displayState() {
    	//setting end of time interval, used for statistics
    	myUber.setEnd(ZonedDateTime.now());
    	//variable used throughout the method
    	BookOfRides myBook = myUber.getMyBook();
    	ComputeStatistics stats = new ComputeStatistics();
    	
    	//beginning of display
		System.out.println("_____");
		System.out.println("");
		System.out.println(" = CURRENT STATE OF THE SYSTEM = ");
		System.out.println("_____");
		System.out.println("");
		//displaying the state of cars
		System.out.println(" == CARS == ");
		System.out.println("");
		System.out.println("Please find below information about the cars present in the system");
		for(Car car : myUber.getCars()) {
			System.out.println("-----");
			System.out.println(car.getID() + " - " + car.getType());
			System.out.println("       | Current Position : " + car.getGPSCoordinates());
			System.out.println("       | Maximum number of passengers: " + car.getMaxPassengers());
			System.out.println("       | Currently being driven by : " + car.getCurrentDriver().getName() + " " + car.getCurrentDriver().getSurname());
    		System.out.println("       | List of its drivers : " + car);
		}
		System.out.println("_____");
		System.out.println("");
		
		//displaying information about the customers
		System.out.println(" == CUSTOMERS == ");
		System.out.println("");
		System.out.println("Please find below information about the customers present in the system");
		for (Customer customer : myUber.getCustomers()) {
			CustomerStatistics customerStats = stats.getCustomerBalance(myBook, customer);
			System.out.println("-----");
			System.out.println(customer.getName() + " " + customer.getSurname() + " - ID : " + customer.getID());
			System.out.println("       | Current Position : " + customer.getGPSCoordinates());
			System.out.println("       | Card number: " + customer.getCardNumber());
			System.out.println("       | Number of rides done : " + customerStats.getNumberOfRides());
			System.out.println("       | Time spent riding : " + customerStats.getTimeSpent());
			System.out.println("       | Total amount paid : " + customerStats.getTotalPaid());
		}
		System.out.println("_____");
		System.out.println("");
		
		//displaying information about the customers
		System.out.println(" == DRIVERS == ");
		System.out.println("");
		System.out.println("Please find below information about the drivers present in the system");
		for (Driver driver  : myUber.getDrivers()) {
			DriverStatistics driverStats = stats.getDriverStatistics(myBook, driver, myUber.getBegin(), myUber.getEnd());
			System.out.println("-----");
			System.out.println(driver.getName() + " " + driver.getSurname() + " - ID : " + driver.getID());
			System.out.println("       | Current Position : " + driver.getCarUsing().getGPSCoordinates());
			System.out.println("       | Car used by driver : " + driver.getCarUsing().getID());
			System.out.println("       | Number of rides done : " + driverStats.getNumberOfRides());
			System.out.println("       | Time spent riding : " + driverStats.getTimeRiding());
			System.out.println("       | Total amount cashed in: " + driverStats.getTotalCashed());
		}
		System.out.println("_____");
		System.out.println("");
	}
    
    /**
     * Outputs the list of prices for every ride type to reach the destination at the specified time for the given customer
     * @param custID
     * @param lat
     * @param lon
     * @param time
     */
    public void ask4price(String custID, String lat, String lon, String time) {
    	//Convert time to Integer 
    	try {
	    	Integer hour = Integer.valueOf(time);
	    	//Get traffic type and make cost factory
	    	TrafficType trafficType;
	    	//if the specified time is correct
	    	if (hour <= 23 && hour >= 0) {
	    		trafficType = myUber.findTrafficType(hour);
	    	//if time is given to compute with the current real time
	    	} else if (hour < 0) {
	    		trafficType = myUber.findTrafficType();
	    	//the given integer makes no sense
	    	} else {
	    		Exception NumberFormatException = new Exception();
				throw NumberFormatException;
	    	}
	    	CostFactory factory = new CostFactory();
	    	//convert destination to double
	    	try {
		    	double convertedLat = Double.parseDouble(lat.replace(",",".") );
		    	double convertedLon = Double.parseDouble(lon.replace(",",".") );
		    	//we attribute the medium elevation of France
		    	double convertedEl = 375;
		    	//generate position
		    	GPSCoordinate position = new GPSCoordinate(convertedLat, convertedLon, convertedEl);
		    	
		    	//finding the specified customer
		    	Customer targetCustomer = null;
		    	for (Customer knownCust : myUber.getCustomers()) {
		    		//converting the id of the customer being iterated on to String for comparison
		    		String IDKnownCust = String.valueOf(knownCust.getID());
		    		if(IDKnownCust.equals(custID)) {
		    			targetCustomer = knownCust;
		    			break;
		    		}
		    	}
		    	if (targetCustomer != null) {
		    		//compute distance
			    	double distance = position.distance(targetCustomer.getGPSCoordinates());
			    	//Printing of information in the CLUI
			    	System.out.println("_____");
			    	System.out.println("Please find below the prices for the different kind of rides : ");
			    	for (RideType type : RideType.values()) {
			    		double cost = factory.getCost(type).getPrice(distance, trafficType);
			    		System.out.println("-----");
			    		System.out.println("The cost to the specified destination, for the " + type + " type of ride, is " + cost + " €.");
			    	}
			    	System.out.println("_____");
		    	} else {
		    		//
		    		System.out.println("_____");
		    		System.out.println("The value you gave for customer ID is not right");
		    		System.out.println("Please make sure you type it correctly, it must be an integer");
		    		System.out.println("_____");
		    	}
	    	} catch (NumberFormatException e) {
	    		//if the given destination does not exist or was mistyped
	    		System.out.println("_____");
	    		System.out.println("The value you gave for latitude or longitude is not right");
	    		System.out.println("Please make sure you type a number, for example : ");
	    		System.out.println("-> 10-10");
	    		System.out.println("-> 10.1-10.1");
	    		System.out.println("-> 10,3-10,3");
	    		System.out.println("_____");
	    	}
    	} catch (Exception e) {
    		//if the given time does not exist or was mistyped
    		System.out.println("_____");
    		System.out.println("The value you gave for time is not right");
    		System.out.println("Please make sure you type an integer, for example : ");
    		System.out.println("-> an integer between 0 and 23 ; for instance, 10, for 10 o'clock");
    		System.out.println("-> any integer < 0, to compute for the current 'real' time");
    		System.out.println("_____");
    	}
	}
    
    /**
     * Simulates a ride with the given parameters and outputs relevant information abour it
     * @param custID
     * @param lat
     * @param lon
     * @param time
     * @param type
     * @param driverMark
     */
    public void simRide(String custID, String lat, String lon, String time, String type, String driverMark) {

    	//Convert time to Integer 
    	try {
	    	Integer hour = Integer.valueOf(time);
	    	//Get traffic type and make cost factory
	    	TrafficType trafficType;
	    	//if the specified time is correct
	    	if (hour <= 23 && hour >= 0) {
	    		trafficType = myUber.findTrafficType(hour);
	    	//if time is given to compute with the current real time
	    	} else if (hour < 0) {
	    		trafficType = myUber.findTrafficType();
	    	//the given integer makes no sense
	    	} else {
	    		Exception NumberFormatException = new Exception();
				throw NumberFormatException;
	    	}
	    	CostFactory factory = new CostFactory();
	    	//convert destination to double
	    	try {
		    	double convertedLat = Double.parseDouble(lat.replace(",",".") );
		    	double convertedLon = Double.parseDouble(lon.replace(",",".") );
		    	//we attribute the medium elevation of France
		    	double convertedEl = 375;
		    	//generate position
		    	GPSCoordinate position = new GPSCoordinate(convertedLat, convertedLon, convertedEl);
		    	
		    	//verifying the rideType
		    	try {
		    		String rideType = "";
		    		switch (type){
		    			case "pool": rideType = "uberX";
		    							break;
		    			case "X" : rideType = "uberBlack";
		    							break;
		    			case "black" : rideType = "uberVan";
		    							break;
		    			case "van" : rideType = "uberVan";
										break;
		    		}
		    		RideType convertedType = RideType.valueOf(rideType);
		    		
		    		try {
		    			Integer mark = Integer.valueOf(driverMark);
		    	    	//if the specified mark makes no sense
		    	    	if (mark < 0 || mark > 5) {
		    	    		Exception NumberFormatException = new Exception();
		    				throw NumberFormatException;
		    	    	}
		    	    	
		    			//finding the specified customer
				    	Customer targetCustomer = null;
				    	for (Customer knownCust : myUber.getCustomers()) {
				    		//converting the id of the customer being iterated on to String for comparison
				    		String IDKnownCust = String.valueOf(knownCust.getID());
				    		if(IDKnownCust.equals(custID)) {
				    			targetCustomer = knownCust;
				    			break;
				    		}
				    	}
				    	if (targetCustomer != null) {
				    		//compute distance
					    	double distance = position.distance(targetCustomer.getGPSCoordinates());
					    	//create array of customers
							ArrayList<Customer> concernedCustomers = new ArrayList<Customer>();
							concernedCustomers.add(targetCustomer);

                            Rides ride1 = new Rides(concernedCustomers, targetCustomer.getGPSCoordinates(), new GPSCoordinate(convertedLat, convertedLon, convertedEl));
							ride1.setType(convertedType);
                            /* Algorithm to find a car */
                            FindCarRunnable findCar = myUber.findCar(ride1);

                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            /* driver2, who has received a notification, accept the ride */
                            findCar.getRide().getCar().getCurrentDriver().acceptRide(trafficType, true, myUber.getMyBook());

                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }




                            findCar.getRide().setDriverMark(mark);


					    	//Printing of information in the CLUI
					    	System.out.println("_____");
					    	System.out.println("Please find below a summary of the ride that just took place: ");
                            System.out.print("Driver found, his name is : ");
                            System.out.println(findCar.getCar().getCurrentDriver().getSurname() + " " + findCar.getCar().getCurrentDriver().getName());
                            System.out.print("The drive will last : ");
                            System.out.println(myUber.getMyBook().getBook().get(0).getDuration());
					    	
					    	System.out.println("_____");
				    	} else {
				    		//
				    		System.out.println("_____");
				    		System.out.println("The value you gave for customer ID is not right");
				    		System.out.println("Please make sure you type it correctly, it must be an integer");
			
				    		System.out.println("_____");
				    	}
		    		} catch (Exception e) {
		    			//if the given time does not exist or was mistyped
						e.printStackTrace();
		        		System.out.println("_____");
		        		System.out.println("The value you gave for driver mark is not right");
		        		System.out.println("Please make sure you type an integer between 0 and 5");
		        		System.out.println("_____");
		    		}
		    	} catch (IllegalArgumentException e) {
		    		//if the given driver status does not exist or was mistyped
		    		System.out.println("_____");
		    		System.out.println("The value you gave for ride type is not right");
		    		System.out.println("Please make sure you type it correctly, with the proper case. As a reminder, the accepted values are : ");
		    		System.out.println("-> pool");
		    		System.out.println("-> X");
		    		System.out.println("-> black");
		    		System.out.println("-> van");
		    		System.out.println("_____");
		    	}
	    	} catch (NumberFormatException e) {
	    		//if the given destination does not exist or was mistyped
	    		System.out.println("_____");
	    		System.out.println("The value you gave for latitude or longitude is not right");
	    		System.out.println("Please make sure you type a number, for example : ");
	    		System.out.println("-> 10-10");
	    		System.out.println("-> 10.1-10.1");
	    		System.out.println("-> 10,3-10,3");
	    		System.out.println("_____");
	    	}
    	} catch (Exception e) {
    		//if the given time does not exist or was mistyped
    		System.out.println("_____");
    		System.out.println("The value you gave for time is not right");
    		System.out.println("Please make sure you type an integer, for example : ");
    		System.out.println("-> an integer between 0 and 23 ; for instance, 10, for 10 o'clock");
    		System.out.println("-> any integer < 0, to compute for the current 'real' time");
    		System.out.println("_____");
    	}
	}
    
    /**
     * Interactive version of simRide
     * @param custID
     * @param lat
     * @param lon
     * @param time
     */
    public void simRide_i(String custID, String lat, String lon, String time) {
    	//OUTPUT 1 : the list of prices for the destination.
    	this.ask4price(custID, lat, lon, time);
    	//INPUT 1 : ask for the kind of ride
        String type = "";
        System.out.println();
        System.out.println("_____");
        System.out.println();
        System.out.println("Please input the type of ride you want to choose ; as a reminder it can be ");
        System.out.println("-> pool");
		System.out.println("-> X");
		System.out.println("-> black");
		System.out.println("-> van");
        System.out.println("_____");
        System.out.println();
        System.out.print("> ");
        //user input
        type = this.scanner.nextLine();
        //Printing of the user choice
        System.out.println("_____");
        System.out.println();
        System.out.println("You chose to do a " + type + " ride !");
        System.out.println();
        System.out.println("_____");
    	//Process ride type given in input
    	try {
    		String rideType = "";
    		switch (type){
    			case "pool": rideType = "uberX";
    							break;
    			case "X" : rideType = "uberBlack";
    							break;
    			case "black" : rideType = "uberVan";
    							break;
    			case "van" : rideType = "uberVan";
								break;
    		}
    		RideType convertedType = RideType.valueOf(rideType);

			double convertedLat = Double.parseDouble(lat.replace(",",".") );
			double convertedLon = Double.parseDouble(lon.replace(",",".") );
			//we attribute the medium elevation of France
			double convertedEl = 375;
			//generate position
			GPSCoordinate position = new GPSCoordinate(convertedLat, convertedLon, convertedEl);
    		
    		//PROCESS make the ride happen
    		
        	//finding the specified customer
        	Customer targetCustomer = null;
        	for (Customer knownCust : myUber.getCustomers()) {
        		//converting the id of the customer being iterated on to String for comparison
        		String IDKnownCust = String.valueOf(knownCust.getID());
        		if(IDKnownCust.equals(custID)) {
        			targetCustomer = knownCust;
        			break;
        		}
        	}
        	if (targetCustomer != null) {
        		
    	    	//INPUT 2 : driver mark
        		String driverMark = "";
        		System.out.println("_____");
                System.out.println();
                System.out.println("Please input the mark (an integer between 0 and 5) you would like to give to the driver. ");
                System.out.println("_____");
                System.out.println();
                System.out.print("> ");
                driverMark = this.scanner.nextLine();
                //printing of the mark the user inputed
                System.out.println("_____");
                System.out.println();
                System.out.println("The mark you gave to your driver is : " + driverMark);
                System.out.println();
                System.out.println("_____");
    	    	//process the mark given in input
                try {
	    			Integer mark = Integer.valueOf(driverMark);
	    	    	//if the specified mark makes no sense
	    	    	if (mark < 0 || mark > 5) {
	    	    		Exception NumberFormatException = new Exception();
	    				throw NumberFormatException;
	    	    	}
					simRide(custID, lat, lon, time, type, driverMark);
	    	    	
	    	    	//OUTPUT final state
	    			this.runMethod("displayState".split(" "));
                } catch (Exception e) {
	    			//if the given time does not exist or was mistyped
	        		System.out.println("_____");
	        		System.out.println("The value you gave for driver mark is not right");
	        		System.out.println("Please make sure you type an integer between 0 and 5");
	        		System.out.println("_____");
	    		}    			
        	} else {
        		//the customer wasn't found
        		System.out.println("_____");
        		System.out.println("The value you gave for customer ID is not right");
        		System.out.println("Please make sure you type it correctly, it must be an integer");

        		System.out.println("_____");
        	}
    	} catch (IllegalArgumentException e) {
    		//if the given driver status does not exist or was mistyped
    		System.out.println("_____");
    		System.out.println("The value you gave for ride type is not right");
    		System.out.println("Please make sure you type it correctly, with the proper case. As a reminder, the accepted values are : ");
    		System.out.println("-> pool");
    		System.out.println("-> X");
    		System.out.println("-> black");
    		System.out.println("-> van");
    		System.out.println("_____");
    	}
	}
    
    /**
     * Displays the drivers sorted according to the given sort policy
     * @param sortPolicy
     */
    public void displayDrivers(String sortPolicy) {
    	//compute statistics related variables
    	BookOfRides myBook = myUber.getMyBook();
        myUber.setEnd(ZonedDateTime.now());
        ComputeStatistics stats = new ComputeStatistics();
        //construct the appropriate list according to the given sorting policy
        List<Entry<Driver, Integer>> rankedDrivers = null;
        String methodSort = "";
        switch (sortPolicy) {
        	case "mostappreciated" : 
        		rankedDrivers = stats.getDriversSortedByAppreciation(myBook);
        		methodSort = "appreciation";
        		break;
        	case "mostoccupied" : 
        		rankedDrivers = stats.getDriversSortedByNbOfRides(myBook);
        		methodSort = "occupation";
        		break;
        }
        
        //if sort policy made sense
        if (rankedDrivers != null) {
        	Integer counter = 1;
        	//printing
        	System.out.println("____");
        	System.out.println();
	    	System.out.println("Please find below all drivers, sorted w.r.t their " + methodSort + " : ");
        	for (Entry<Driver, Integer> driverInfo : rankedDrivers) {
        		Driver driver = driverInfo.getKey();
        		Integer valueUponSort = driverInfo.getValue();
        		//printing information about the driver
        		System.out.println("-----");
    			System.out.println("Place" + counter + " : " + driver.getName() + " " + driver.getSurname() );
    			System.out.println("       | Driver ID : " + driver.getID());
    			System.out.println("       | Car used by driver : " + driver.getCarUsing().getID());
    			System.out.println("       | Value according to which sorted : " + valueUponSort);
    			//increment counter
    			counter ++;
        	}
        	
        	
        } else {
        	System.out.println("_____");
    		System.out.println("The value you gave for sort policy is not right");
    		System.out.println("Please make sure you type it correctly, it must be one of the two strings listed below");
    		System.out.println("-> mostoccupied");
    		System.out.println("-> mostappreciated");
    		System.out.println("_____");
        }
        
	}
    
    /**
     * Displays the drivers sorted according to the given sort policy
     * @param sortPolicy
     */
    public void displayCustomers(String sortPolicy) {
    	//compute statistics related variables
    	BookOfRides myBook = myUber.getMyBook();
        myUber.setEnd(ZonedDateTime.now());
        ComputeStatistics stats = new ComputeStatistics();
        //construct the appropriate list according to the given sorting policy
        List<Entry<Customer, Integer>> rankedCustomersFreq = null;
        List<Entry<Customer, Double>> rankedCustomersPaid = null;
        String methodSort = null;
        switch (sortPolicy) {
        	case "mostfrequent" : 
        		rankedCustomersFreq = stats.getCustomersSortedByNbOfRides(myBook);
        		methodSort = "frequency";
        		break;
        	case "mostcharged" : 
        		rankedCustomersPaid = stats.getCustomersSortedByAmount(myBook);
        		methodSort = "amount paid";
        		break;
        }
        
        //if sort policy made sense
        if (methodSort != null) {
        	Integer counter = 1;
        	//printing
        	System.out.println("____");
        	System.out.println();
	    	System.out.println("Please find below all drivers, sorted w.r.t their " + methodSort + " : ");
	    	if (rankedCustomersFreq != null) {
	        	for (Entry<Customer, Integer> customerInfo : rankedCustomersFreq) {
	        		Customer customer = customerInfo.getKey();
	        		Integer valueUponSort = customerInfo.getValue();
	        		//printing information about the driver
	        		System.out.println("-----");
	    			System.out.println("Place" + counter + " : " + customer.getName() + " " + customer.getSurname() );
	    			System.out.println("       | Customer ID : " + customer.getID());
	    			System.out.println("       | Value according to which sorted : " + valueUponSort);
	    			//increment counter
	    			counter ++;
	        	}
	    	} else if (rankedCustomersPaid != null) {
	    		for (Entry<Customer, Double> customerInfo : rankedCustomersPaid) {
	        		Customer customer = customerInfo.getKey();
	        		Double valueUponSort = customerInfo.getValue();
	        		//printing information about the driver
	        		System.out.println("-----");
	    			System.out.println("Place" + counter + " : " + customer.getName() + " " + customer.getSurname() );
	    			System.out.println("       | Customer ID : " + customer.getID());
	    			System.out.println("       | Value according to which sorted : " + valueUponSort);
	    			//increment counter
	    			counter ++;
	        	}
	    	}
        	
        } else {
        	System.out.println("_____");
    		System.out.println("The value you gave for sort policy is not right");
    		System.out.println("Please make sure you type it correctly, it must be one of the two strings listed below");
    		System.out.println("-> mostcharged");
    		System.out.println("-> mostfrequent");
    		System.out.println("_____");
        }
        
	}
    
    /**
     * Displays the total amount cashed in by drivers
     */
    public void totalCashed() {
    	//compute statistics related variables
    	BookOfRides myBook = myUber.getMyBook();
        myUber.setEnd(ZonedDateTime.now());
        ComputeStatistics stats = new ComputeStatistics();
        double totalCashed = stats.getSystemBalance(myBook).getTotalAmountCashedByAllDrivers();
        //printing information
        System.out.println("_____");
        System.out.println();
		System.out.println("Total Amount cashed in by all drivers : " + totalCashed + " €.");
		System.out.println("_____");
	}
    
    /**
     * Main function
     */
    public static void main(String [] args) {
        CLUI clui = new CLUI();
        clui.run();
    }
}
