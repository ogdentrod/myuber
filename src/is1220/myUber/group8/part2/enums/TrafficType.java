package is1220.myUber.group8.part2.enums;

/**
 * Enumeration for the type of traffic
 * @author Cécile Boniteau et Benoit Sepe
 * @version 1.0
 */
public enum TrafficType {
    LOW_TRAFFIC,
    MEDIUM_TRAFFIC,
    HEAVY_TRAFFIC,
}
