package is1220.myUber.group8.part2.enums;

/**
 * Enumeration of the different statuses a driver can be on
 * @author Cécile Boniteau et Benoît Sepe
 * @version 1.0
 */
public enum DriverState {
    OFFLINE,
    ON_DUTY,
    OFF_DUTY,
    ON_A_RIDE

}
