package is1220.myUber.group8.part2.enums;

/**
 * Enumeration of the different statuses a ride can be.
 * @author Cécile Boniteau et Benoît Sepe
 * @version 1.0
 */
public enum RideStatus {
    unconfirmed,
    confirmed,
    ongoing,
    canceled,
    completed,
}
