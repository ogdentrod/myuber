package is1220.myUber.group8.part2;
import java.lang.System;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class to represent GPS position an compute distances
 * @author Cécile Boniteau et Benoît Sepe
 * @version 1.0
 */
public class GPSCoordinate {

    private double lat;
    private double lon;
    private double el;
    
    public GPSCoordinate() {
	}
    /**
     * Constructor
     * @param lat latitude
     * @param lon longitude
     * @param el elevation (height)
     */
    public GPSCoordinate(double lat, double lon, double el) {
        this.lat = lat;
        this.lon = lon;
        this.el = el;
    }
    
    /**
     * Generate random coordinates with lat, lon between min and max, and el between 0 and 1
     * @param min
     * @param max
     * @return
     */
    public static GPSCoordinate randomCoordinates(double min, double max) {
    	Random r = new Random(System.currentTimeMillis());
    	double randomLat = min + (max - min) * r.nextDouble();
    	double randomLon = ThreadLocalRandom.current().nextDouble(min, max);
    	double randomEl = ThreadLocalRandom.current().nextDouble(0, 1);
    	GPSCoordinate coord = new GPSCoordinate(randomLat, randomLon, randomEl);
    	return coord;
    }
    /**
     * Method to get the distance between to points in kilometers
     * @param gps a gps coordinate
     * @return
     */
    public double distance(GPSCoordinate gps) {
        // I suppose that a straight road connect the starting and ending point

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(gps.lat - this.lat);
        double lonDistance = Math.toRadians(gps.lon - this.lon);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(this.lat)) * Math.cos(Math.toRadians(gps.lat))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c *1000; // convert in meters

        double height = this.el - gps.el;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);
        distance = Math.sqrt(distance) / 1000; // convert to kilometers
        return distance;
    }


    public GPSCoordinate minDistance(ArrayList<GPSCoordinate> pointsR) {
        ArrayList<GPSCoordinate> points = new ArrayList<GPSCoordinate>(pointsR);
        GPSCoordinate minPoint = points.remove(0);
        double distanceMin = this.distance(minPoint);
        for (GPSCoordinate point : points) {
            double dist = this.distance(point);
            if (dist < distanceMin) {
                distanceMin = dist;
                minPoint = point;
            }
        }
        return minPoint;
    }

    @Override
    public String toString()
    {
        return "Lat: " + this.lat + " - Lon: " + this.lon + " - El: " + this.el;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getEl() {
        return el;
    }

    public void setEl(double el) {
        this.el = el;
    }
    
}
