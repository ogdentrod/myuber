package is1220.myUber.group8.part2.humans;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing all customers interacting with myUber
 * @author Cécile Boniteau and Benoît Sepe
 * @version 1.0
 */
public class Customer extends Human {
    private GPSCoordinate GPSCoordinates;
    private String cardNumber; // We should encrypt that
    private List<Message> messageBox;
    
    public Customer () {}
    /**
     * Constructor.
     * @param GPSCoordinates Position of the customer
     * @param cardNumber Credit card number to which to bill the customer
     * @param messageBox The message box inside of which a customer can find the messages of myUber
     */
    public Customer(String name, String surname, GPSCoordinate GPSCoordinates, String cardNumber, ArrayList<Message> messageBox) {
        super(name, surname);
        this.GPSCoordinates = GPSCoordinates;
        this.cardNumber = cardNumber;
        this.messageBox = messageBox;
    }

    /**
     * Getter for GPSCoordinates
     */
    public GPSCoordinate getGPSCoordinates() {
        return this.GPSCoordinates;
    }
    /**
     * Setter for GPSCoordinates
     * @param GPSCoordinates The position of the customer
     */
    public void setGPSCoordinates(GPSCoordinate GPSCoordinates) {
        this.GPSCoordinates = GPSCoordinates;
    }
    
    /**
     * Getter for the card number
     */
    public String getCardNumber() {
        return this.cardNumber;
    }
    
    /**
     * Setter for the card number
     * @param cardNumber  Credit card number to which to bill the customer
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    /**
     * Getter for the message box
     */
    public List<Message> getMessageBox() {
        return this.messageBox;
    }
    
    /**
     * Setter for the message box
     * @param messageBox The message box inside of which a customer can find the messages of myUbers
     */
    public void setMessageBox(List<Message> messageBox) {
        this.messageBox = messageBox;
    }
    
    /**
     * Method for adding a message to the message box of a customer
     * @param message The message that ought to be added to the message box
     */
    public void addMessage(Message message) {
        this.messageBox.add(message);
    }
    
    /**
     * Method for removing a message from the message box
     * @param message The message that ought to be removed from the message box
     */
    public void removeMessage(Message message) {
        this.messageBox.remove(message);
    }
    
    /**
     * Method clearing the message box from all messages
     */
    public void clearMessageBox() {
        this.messageBox.clear();
    }
}
