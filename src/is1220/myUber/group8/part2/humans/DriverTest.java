package is1220.myUber.group8.part2.humans;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.enums.DriverState;

class DriverTest extends Driver {

	@Test
	void testGetState() {
		 Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		 assertEquals(driver1.getState(), DriverState.ON_DUTY);
	}

	@Test
	void testSetState() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.OFF_DUTY);
		driver1.setState(DriverState.ON_DUTY);
		assertEquals(driver1.getState(), DriverState.ON_DUTY);
	}

	@Test
	void testGetName() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		assertEquals(driver1.getName(), "Smith1");
	}

	@Test
	void testSetName() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		driver1.setName("B. Good");
		assertEquals(driver1.getName(), "B. Good");
	}

	@Test
	void testGetSurname() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		assertEquals(driver1.getSurname(), "John1");
	}

	@Test
	void testSetSurname() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		driver1.setSurname("Johnny");
		assertEquals(driver1.getSurname(), "Johnny");
	}

	@Test
	void testGetID() {
		Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
		assertTrue(driver1.getID() <= compteur);
	}

}
