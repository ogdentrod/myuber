package is1220.myUber.group8.part2.humans;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.Message;

class CustomerTest extends Customer {

	@Test
	void testGetGPSCoordinates() {
		GPSCoordinate position = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		Customer customer = new Customer("Sepe", "Benoît", position, "4944184419592889", new ArrayList<Message>());
		assertEquals(position, customer.getGPSCoordinates());
	}

	@Test
	void testSetGPSCoordinates() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(0, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		GPSCoordinate newPosition = new GPSCoordinate(48.7100841, 2.163283299999989, 158);
		customer.setGPSCoordinates(newPosition);
		assertEquals(newPosition, customer.getGPSCoordinates());
	}

	@Test
	void testGetCardNumber() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		assertEquals("4944184419592889", customer.getCardNumber());
	}

	@Test
	void testSetCardNumber() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4942889", new ArrayList<Message>());
		customer.setCardNumber("4944184419592889");
		assertEquals("4944184419592889", customer.getCardNumber());
	}

	@Test
	void testAddMessage() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4942889", new ArrayList<Message>());
		@SuppressWarnings("deprecation")
		Message message = new Message(new Date(1997, 8, 13), "Ceci est un test");
		customer.addMessage(message);
		assertEquals(customer.getMessageBox().size(), 1);
		
	}

	@Test
	void testRemoveMessage() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4942889", new ArrayList<Message>());
		@SuppressWarnings("deprecation")
		Message message = new Message(new Date(1997, 8, 13), "Ceci est un test");
		customer.addMessage(message);
		customer.removeMessage(message);
		assertEquals(customer.getMessageBox().size(), 0);

	}

	@Test
	void testClearMessageBox() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4942889", new ArrayList<Message>());
		@SuppressWarnings("deprecation")
		Message message = new Message(new Date(1997, 8, 13), "Ceci est un test");
		customer.addMessage(message);
		customer.clearMessageBox();
		assertEquals(customer.getMessageBox().size(), 0);
	}
	
	@Test
	void testGetName() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		assertEquals(customer.getName(), "Sepe");
	}

	@Test
	void testSetName() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		customer.setName("B. Good");
		assertEquals(customer.getName(), "B. Good");
	}

	@Test
	void testGetSurname() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		assertEquals(customer.getSurname(), "Benoît");
	}

	@Test
	void testSetSurname() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		customer.setSurname("Johnny");
		assertEquals(customer.getSurname(), "Johnny");
	}

	@Test
	void testGetID() {
		Customer customer = new Customer("Sepe", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
		assertTrue(customer.getID() <= compteur);
	}

}
