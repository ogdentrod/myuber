package is1220.myUber.group8.part2.humans;

import is1220.myUber.group8.part2.BookOfRides;
import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.enums.DriverState;
import is1220.myUber.group8.part2.enums.TrafficType;

/**
 * Class representing all drivers interacting with myUber
 * @author Cécile Boniteau and Benoît Sepe
 * @version 1.0
 */
public class Driver extends Human{

	private DriverState state;
	private Car carUsing;
	
	public Driver() {}
	/**
	 * Constructor
	 * @param state State of the current driver (whether he is on duty or not, etc.)
	 */
    public Driver(String name, String surname, DriverState state) {
        super(name, surname);
        this.state = state;
        this.carUsing = null;
    }

    public void acceptRide(TrafficType trafficType, boolean accepted, BookOfRides bookOfRides) {
        carUsing.acceptRide(trafficType, accepted, bookOfRides);
    }
    
    /**
     * Getter for the driver's state
     */
    public DriverState getState() {
        return state;
    }
    
    /**
     * Setter for the driver's state
     * @param state State of the current driver (whether he is on duty or not, etc.)
     */
    public void setState(DriverState state) {
        this.state = state;
    }

    public Car getCarUsing() {
        return carUsing;
    }

    public void setCarUsing(Car carUsing) {
        this.carUsing = carUsing;
    }
}
