package is1220.myUber.group8.part2.humans;

/**
 * Class representing all humans interacting with myUber
 * @author Cécile Boniteau and Benoît Sepe
 * @version 1.0
 */
public class Human {
    private String name;
    private String surname;
    private int ID;

    static int compteur = 1;
    
    public Human () {}
    /**
     * Constructor. ALso increments the global counter
     * @param name The name of the human
     * @param surname The surname of the human
     */
    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.ID = compteur;
        compteur++;
    }
    
    /**
     * Getter of the human name
     * @return name The name of the considered human
     */
    public String getName() {
        return name;
    }
    
    /**
     * Setter for the name
     * @param name The name one wants to give to a human object
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Getter for a human surname
     * @return surname The surname of the considered human
     */
    public String getSurname() {
        return surname;
    }
    
    /**
     * Setter for a human surname
     * @param surname The surname one wants to give to a human object
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }
    
    /**
     * Getter for a human's ID
     * @return ID An integer representing a human's unique ID
     */
    public int getID() {
        return ID;
    }
    
    /**
     * Setter for a human's ID
     * @param ID An integer representing a human's unique ID
     */
    public void setID(int ID) {
        this.ID = ID;
    }
}
