package is1220.myUber.group8.part2.cost;

import is1220.myUber.group8.part2.enums.TrafficType;

/**
 * Class for computing the basic rate and the traffic rate of UberPool rides.
 * @author Cécile Boniteau et Benoît Sepe
 * @version 1.0
 */
public class UberPoolCost extends Cost {
	
	/**
	 * Computing of cost for this particular type of ride
	 * @see Cost
	 */
    @Override
    public double getBasicRate(double length) {
        if (length < 5) {
            return 2.4;
        } else if (length < 10) {
            return 3;
        } else if (length < 20) {
            return 1.3;
        } else {
            return 1.1;
        }
    }
    
    /**
	 * Computing of cost for this particular type of ride
	 * @see Cost
	 */
    @Override
    public double getTrafficRate(TrafficType trafficType) {
        if(trafficType == TrafficType.LOW_TRAFFIC) {
            return 1;
        } else if (trafficType == TrafficType.MEDIUM_TRAFFIC) {
            return 1.1;
        } else if (trafficType == TrafficType.HEAVY_TRAFFIC) {
            return 1.2;
        }
        return 0;
    }
}
