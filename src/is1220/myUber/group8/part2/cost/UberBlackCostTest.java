/**
 * 
 */
package is1220.myUber.group8.part2.cost;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.enums.TrafficType;

/**
 * @author Benoit Sepe et Cécile Boniteau
 *
 */
class UberBlackCostTest extends UberBlackCost{

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.UberBlackCost#getBasicRate(double)}.
	 */
	@Test
	void testGetBasicRate() {
		UberBlackCost cost = new UberBlackCost();
		//TESTS
		assertEquals(cost.getBasicRate(3), 6.2);
		assertEquals(cost.getBasicRate(7), 5.5);
		assertEquals(cost.getBasicRate(13), 3.25);
		assertEquals(cost.getBasicRate(27), 2.6);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.UberBlackCost#getTrafficRate(is1220.myUber.group8.part2.enums.TrafficType)}.
	 */
	@Test
	void testGetTrafficRate() {
		UberBlackCost cost = new UberBlackCost();
		//TESTS
		assertEquals(cost.getTrafficRate(TrafficType.LOW_TRAFFIC),1);
		assertEquals(cost.getTrafficRate(TrafficType.MEDIUM_TRAFFIC),1.3);
		assertEquals(cost.getTrafficRate(TrafficType.HEAVY_TRAFFIC),1.6);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.Cost#getPrice(double, is1220.myUber.group8.part2.enums.TrafficType)}.
	 */
	@Test
	void testGetPrice() {
		UberBlackCost cost = new UberBlackCost();
		//TEST
		assertEquals(cost.getPrice(13, TrafficType.HEAVY_TRAFFIC), 13*3.25*1.6);
		//Price = BasicRate(length) * length * TrafficRate(trafficType);
	}

}
