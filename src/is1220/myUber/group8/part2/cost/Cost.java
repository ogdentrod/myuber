package is1220.myUber.group8.part2.cost;

import is1220.myUber.group8.part2.enums.TrafficType;

/**
 * Abstract class for the computing of the cost of rides.
 * @author Cécile Boniteau et Benoît Sepe
 * @version 1.0
 */
public abstract class Cost {
	
	/**
	 * Getter for the basic rate
	 * @param length Distance of the ride
	 */
    public abstract double getBasicRate(double length);
    
    /**
     * Getter for the traffic rate, which depends on the kind of traffic
     * @param trafficType The current state of the traffic
     */
    public abstract double getTrafficRate(TrafficType trafficType);
    
    /**
     * This methods compute the price of ride
     * @param length The distance of the ride
     * @param trafficType the current state of the traffic
     * @return
     */
    public double getPrice(double length, TrafficType trafficType) {
        return getBasicRate(length) * length * getTrafficRate(trafficType);
    }

}
