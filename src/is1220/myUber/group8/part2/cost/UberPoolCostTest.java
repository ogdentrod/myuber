/**
 * 
 */
package is1220.myUber.group8.part2.cost;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.enums.TrafficType;

/**
 * @author proxyma
 *
 */
class UberPoolCostTest extends UberPoolCost {

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.UberPoolCost#getBasicRate(double)}.
	 */
	@Test
	void testGetBasicRate() {
		UberPoolCost cost = new UberPoolCost();
		//TESTS
		assertEquals(cost.getBasicRate(3), 2.4);
		assertEquals(cost.getBasicRate(7), 3);
		assertEquals(cost.getBasicRate(13), 1.3);
		assertEquals(cost.getBasicRate(27), 1.1);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.UberPoolCost#getTrafficRate(is1220.myUber.group8.part2.enums.TrafficType)}.
	 */
	@Test
	void testGetTrafficRate() {
		UberPoolCost cost = new UberPoolCost();
		//TESTS
		assertEquals(cost.getTrafficRate(TrafficType.LOW_TRAFFIC),1);
		assertEquals(cost.getTrafficRate(TrafficType.MEDIUM_TRAFFIC),1.1);
		assertEquals(cost.getTrafficRate(TrafficType.HEAVY_TRAFFIC),1.2);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.Cost#getPrice(double, is1220.myUber.group8.part2.enums.TrafficType)}.
	 */
	@Test
	void testGetPrice() {
		UberPoolCost cost = new UberPoolCost();
		//TEST
		assertEquals(cost.getPrice(13, TrafficType.HEAVY_TRAFFIC), 13*1.3*1.2);
		//Price = BasicRate(length) * length * TrafficRate(trafficType);
	}

}
