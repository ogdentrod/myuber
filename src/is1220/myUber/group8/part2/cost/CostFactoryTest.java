/**
 * 
 */
package is1220.myUber.group8.part2.cost;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.enums.RideType;

/**
 * @author Cécile Boniteau and Benoît Sepe
 *
 */
class CostFactoryTest extends CostFactory {


	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.CostFactory#getCost(is1220.myUber.group8.part2.enums.RideType)}.
	 */
	@Test
	void testGetCost() {
		CostFactory factory = new CostFactory();
		//Return of getCost
		Cost supposedVan = factory.getCost(RideType.uberVan);
		Cost supposedPool = factory.getCost(RideType.uberPool);
		Cost supposedX = factory.getCost(RideType.uberX);
		Cost supposedBlack = factory.getCost(RideType.uberBlack);
		//Real costs
		UberVanCost van = new UberVanCost();
		UberBlackCost black = new UberBlackCost();
		UberPoolCost pool = new UberPoolCost();
		UberXCost x = new UberXCost();
		//TESTS
		assertEquals(supposedBlack.getClass(), black.getClass());
		assertEquals(supposedVan.getClass(), van.getClass());
		assertEquals(supposedPool.getClass(), pool.getClass());
		assertEquals(supposedX.getClass(), x.getClass());
	}

}
