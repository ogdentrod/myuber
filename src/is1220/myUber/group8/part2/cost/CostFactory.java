package is1220.myUber.group8.part2.cost;

import is1220.myUber.group8.part2.AbstractFactory;
import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.humans.Driver;
import is1220.myUber.group8.part2.enums.RideType;

import java.util.List;

public class CostFactory extends AbstractFactory {
	/**
	 * @see AbstractFactory
	 */
    @Override
    public Car getCar(RideType rideType, List<Driver> drivers, Driver currentDriver, GPSCoordinate GPSCoordinates) {
        return null;
    }
    
    /**
     * Factory for the computation of the cost of a certain ride, depending on its type
     * @see AbstractFactory
     */
    @Override
    public Cost getCost(RideType rideType) {
        if (rideType == RideType.uberX) {
            return new UberXCost();
        } else if (rideType == RideType.uberPool) {
            return new UberPoolCost();
        } else if (rideType == RideType.uberBlack) {
            return new UberBlackCost();
        } else if (rideType == RideType.uberVan) {
            return new UberVanCost();
        }
        return null;
    }

}
