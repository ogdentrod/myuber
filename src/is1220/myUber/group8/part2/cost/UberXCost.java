package is1220.myUber.group8.part2.cost;

import is1220.myUber.group8.part2.enums.TrafficType;

/**
 * Class for computing the basic rate and the traffic rate of UberX rides.
 * @author Cécile Boniteau et Benoît Sepe
 * @version 1.0
 */
public class UberXCost extends Cost {
	/**
	 * Computing of cost for this particular type of ride
	 * @see Cost
	 */
    @Override
    public double getBasicRate(double length) {
        if (length < 5) {
            return 3.3;
        } else if (length < 10) {
            return 4.2;
        } else if (length < 20) {
            return 1.91;
        } else {
            return 1.5;
        }
    }
    
    /**
	 * Computing of cost for this particular type of ride
	 * @see Cost
	 */
    @Override
    public double getTrafficRate(TrafficType trafficType) {
        if(trafficType == TrafficType.LOW_TRAFFIC) {
            return 1;
        } else if (trafficType == TrafficType.MEDIUM_TRAFFIC) {
            return 1.1;
        } else if (trafficType == TrafficType.HEAVY_TRAFFIC) {
            return 1.5;
        }
        return 0;
    }
}
