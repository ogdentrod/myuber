/**
 * 
 */
package is1220.myUber.group8.part2.cost;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.enums.TrafficType;

/**
 * @author Cécile Boniteau et Benoît Sepe
 *
 */
class UberXCostTest extends UberXCost {

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.UberXCost#getBasicRate(double)}.
	 */
	@Test
	final void testGetBasicRate() {
		UberXCost cost = new UberXCost();
		//TESTS
		assertEquals(cost.getBasicRate(3), 3.3);
		assertEquals(cost.getBasicRate(7), 4.2);
		assertEquals(cost.getBasicRate(13), 1.91);
		assertEquals(cost.getBasicRate(27), 1.5);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.UberXCost#getTrafficRate(is1220.myUber.group8.part2.enums.TrafficType)}.
	 */
	@Test
	final void testGetTrafficRate() {
		UberXCost cost = new UberXCost();
		//TESTS
		assertEquals(cost.getTrafficRate(TrafficType.LOW_TRAFFIC),1);
		assertEquals(cost.getTrafficRate(TrafficType.MEDIUM_TRAFFIC),1.1);
		assertEquals(cost.getTrafficRate(TrafficType.HEAVY_TRAFFIC),1.5);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.Cost#getPrice(double, is1220.myUber.group8.part2.enums.TrafficType)}.
	 */
	@Test
	final void testGetPrice() {
		UberXCost cost = new UberXCost();
		//TEST
		assertEquals(cost.getPrice(13, TrafficType.HEAVY_TRAFFIC), 13*1.91*1.5);
		//Price = BasicRate(length) * length * TrafficRate(trafficType);
	}

}
