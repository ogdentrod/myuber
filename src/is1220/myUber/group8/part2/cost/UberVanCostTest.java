package is1220.myUber.group8.part2.cost;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import is1220.myUber.group8.part2.enums.TrafficType;

class UberVanCostTest extends UberVanCost {
	
	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.UberVanCost#getBasicRate(double)}.
	 */
	@Test
	void testGetBasicRate() {
		UberVanCost cost = new UberVanCost();
		//TESTS
		assertEquals(cost.getBasicRate(3), 6.2);
		assertEquals(cost.getBasicRate(7), 7.7);
		assertEquals(cost.getBasicRate(13), 3.25);
		assertEquals(cost.getBasicRate(27), 2.6);
	}
	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.UberVanCost#getTrafficRate(is1220.myUber.group8.part2.enums.TrafficType)}.
	 */
	@Test
	void testGetTrafficRate() {
		UberVanCost cost = new UberVanCost();
		//TESTS
		assertEquals(cost.getTrafficRate(TrafficType.LOW_TRAFFIC),1);
		assertEquals(cost.getTrafficRate(TrafficType.MEDIUM_TRAFFIC),1.5);
		assertEquals(cost.getTrafficRate(TrafficType.HEAVY_TRAFFIC),1.8);
	}

	/**
	 * Test method for {@link is1220.myUber.group8.part2.cost.Cost#getPrice(double, is1220.myUber.group8.part2.enums.TrafficType)}.
	 */
	@Test
	void testGetPrice() {
		UberVanCost cost = new UberVanCost();
		//TEST
		assertEquals(cost.getPrice(13, TrafficType.HEAVY_TRAFFIC), 13*3.25*1.8);
		//Price = BasicRate(length) * length * TrafficRate(trafficType);
	}

}
