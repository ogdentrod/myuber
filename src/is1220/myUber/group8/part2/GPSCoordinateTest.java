/**
 * 
 */
package is1220.myUber.group8.part2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * @author Cécile Boniteau et Benoît Sepe
 *
 */
class GPSCoordinateTest extends GPSCoordinate {

	/**
	 * Test method for {@link is1220.myUber.group8.part2.GPSCoordinate#distance(is1220.myUber.group8.part2.GPSCoordinate)}.
	 * 10 meters of incertitude is tolerated
	 */
	@Test
	final void testDistance() {
		GPSCoordinate coord1 = new GPSCoordinate(0, 0, 0);
    	GPSCoordinate coord2 = new GPSCoordinate(1, 1, 0);
    	assertTrue( 157.24 <= coord1.distance(coord2) && coord1.distance(coord2) <= 157.25);
	}

}
