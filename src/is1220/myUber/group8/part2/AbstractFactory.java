package is1220.myUber.group8.part2;

import java.util.List;

import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.cost.Cost;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Driver;

/**
 * Abstract class to make the factories used in the different factory pattern used in the project
 * @author Benoit Sepe and Cécile Boniteau
 * @version 1.0
 */
public abstract class AbstractFactory {
	/**
	 * Getter of CarFactory
	 * @param rideType This is the type of the ride the car will be used
	 * @param drivers This is the list of possible drivers
	 * @param currentDriver This is the driver of the car being constructed that will be the one using it.
	 * @param GPSCoordinates The position of the car.
	 * @return car Object of class Berline, Van or Standard (that extends abstract class Car)
	 */
    public abstract Car getCar(RideType rideType, List<Driver> drivers, Driver currentDriver, GPSCoordinate GPSCoordinates);
    
    /**
     * Getter of the CostFactory
     * @param rideType The type of ride to compute the cost of.
     * @return 
     */
    public abstract Cost getCost(RideType rideType);
}
