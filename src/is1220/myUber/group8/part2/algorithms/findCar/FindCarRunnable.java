package is1220.myUber.group8.part2.algorithms.findCar;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.Rides;
import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.enums.RideStatus;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Customer;

import java.util.ArrayList;
import java.util.List;

public class FindCarRunnable implements Runnable {
    private Rides ride;
    private ArrayList<Rides> rides;
    private ArrayList<Car> cars;

    private FindCar findCar;

    Car car;


    public FindCarRunnable(ArrayList<Rides> rides, ArrayList<Car> cars) {
        this.rides = rides;
        this.cars = cars;
        this.car = null;
        findCar = new FindCar();

        if (rides.size() > 1) {
            ArrayList<Customer> customers = new ArrayList<Customer>();
            for (Rides ride : rides) {
                customers.add(ride.getCustomers().get(0));
            }

            ride = new Rides(customers);
            ride.setType(RideType.uberPool);
            ride.setCustomers(customers);

            this.sortRides();

        } else {
            ride = rides.get(0);
        }


    }

    public Car getCar() {
        return car;
    }
    public Rides getRide() {
        return ride;
    }

    @Override
    public  void run() {

        List<Car> exclude = new ArrayList<Car>();


        Car carTemp = null;

        synchronized(ride) {
            while (ride.getStatus() != RideStatus.confirmed) {

                if (carTemp != null) exclude.add(carTemp);
                carTemp = this.findCar.findCar(this.cars, ride, exclude);
                if (carTemp == null) break;
                ride.setCar(carTemp);
                ride.setStatus(RideStatus.unconfirmed);
                carTemp.setRide(ride);
                if (ride.getType() == RideType.uberPool) sortRides();

                while(ride.getStatus() == RideStatus.unconfirmed) {
                    try {
                        ride.wait();
                    } catch (InterruptedException e) {
                        ride.setStatus(RideStatus.canceled);
                        break;
                    }
                }

            }
            this.car = carTemp;
        }

    }


    // ABRACADABRA, sort GPS points for uber pool
    public void sortRides() {
        ride.setPoints(new ArrayList<GPSCoordinate>());
        ArrayList<Rides> toSort = new ArrayList<Rides>(rides);

        ride.setDestinationPoint(null);
        ride.setStartingPoint(null);

        // First we deal with the starting points
        ArrayList<GPSCoordinate> points = new ArrayList<GPSCoordinate>();
        for (Rides rideToSort : toSort) {
            points.add(rideToSort.getStartingPoint());
        }

        GPSCoordinate previous;

        if (ride.getCar() == null) {
            previous = rides.get(0).getStartingPoint();
        } else {
            previous = ride.getCar().getGPSCoordinates();
        }

        while (points.size() > 0) {
            GPSCoordinate point = previous.minDistance(points);
            if (ride.getStartingPoint() == null) {
                ride.setStartingPoint(point);
            } else {
                ride.addPoints(point);
            }

            points.remove(point);
            previous = point;
        }

        // Then, with the destinations points
        points = new ArrayList<GPSCoordinate>();
        for (Rides rideToSort : toSort) {
            points.add(rideToSort.getDestinationPoint());
        }


        while (points.size() > 0) {
            GPSCoordinate point = previous.minDistance(points);
            if (points.size() == 1) {

                ride.setDestinationPoint(point);
            } else {
                ride.addPoints(point);
            }

            points.remove(point);
            previous = point;
        }

    }




}
