package is1220.myUber.group8.part2.algorithms.findCar;

import is1220.myUber.group8.part2.Rides;
import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.enums.DriverState;

import java.util.List;

public class FindCar {

    public Car findCar(List<Car> cars, Rides ride, List<Car> exclude) {
        Car carMin = null;
        double distanceMin = -1; // distance can't be negative, so we use it as initialisation
        for (Car car : cars) {
            if (!exclude.contains(car) && car.getType().contains(ride.getType()) && car.getCurrentDriver().getState() == DriverState.ON_DUTY) {
                double distance = car.getGPSCoordinates().distance(ride.getStartingPoint());
                if (distanceMin < 0 || distance < distanceMin) {
                    carMin = car;
                    distanceMin = distance;
                }
            }
        }
        return carMin;
    }



}
