package is1220.myUber.group8.part2.algorithms.findCar;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.Message;
import is1220.myUber.group8.part2.Rides;
import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.cars.Standard;
import is1220.myUber.group8.part2.enums.DriverState;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Customer;
import is1220.myUber.group8.part2.humans.Driver;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class FindCarTest {

    Car car1, car2;
    ArrayList<Car> cars;
    Customer cust;
    ArrayList<Customer> customers;
    Rides ride;
    FindCar findCar;

    Car createCar(GPSCoordinate gps) {
        Driver driver = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers = new ArrayList<Driver>();
        drivers.add(driver);
        return new Standard(drivers, driver, gps);
    }

    @BeforeEach
    void setUp() {
        car1 = createCar(new GPSCoordinate(65, 10, 100));
        car2 = createCar(new GPSCoordinate(70, 2, 100));

        cars = new ArrayList<Car>();
        cars.add(car1);
        cars.add(car2);

        cust = new Customer("Sepe1", "Benoît", new GPSCoordinate(80, 2, 100), "4944184419592889", new ArrayList<Message>());

        customers = new ArrayList<Customer>();
        customers.add(cust);

        ride = new Rides(customers, cust.getGPSCoordinates(), new GPSCoordinate(51.7262433, 2.365247199999999, 84));
        ride.setType(RideType.uberPool);

        findCar = new FindCar();
    }

    @org.junit.jupiter.api.Test
    void findCar() {

        assertEquals(findCar.findCar(cars, ride, new ArrayList<Car>()), car2);

    }

    @org.junit.jupiter.api.Test
    void findCarExclude() {
        ArrayList<Car> exclude = new ArrayList<Car>();
        exclude.add(car2);

        assertEquals(findCar.findCar(cars, ride, exclude), car1);

    }
}