package is1220.myUber.group8.part2.algorithms.findCar;

import is1220.myUber.group8.part2.GPSCoordinate;
import is1220.myUber.group8.part2.Message;
import is1220.myUber.group8.part2.Rides;
import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.cars.Standard;
import is1220.myUber.group8.part2.enums.DriverState;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.humans.Customer;
import is1220.myUber.group8.part2.humans.Driver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class FindCarRunnableTest {

    FindCarRunnable findCar;
    ArrayList<Customer> customers1, customers2, customers3;
    ArrayList<Rides> rides;

    @BeforeEach
    void setUp() {
        Customer cust1 = new Customer("Sepe1", "Benoît", new GPSCoordinate(48.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
        Customer cust2 = new Customer("Sepe2", "Benoît", new GPSCoordinate(49.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());
        Customer cust3 = new Customer("Sepe3", "Benoît", new GPSCoordinate(50.7100841, 2.163283299999989, 158), "4944184419592889", new ArrayList<Message>());

        customers1 = new ArrayList<Customer>();
        customers1.add(cust1);

        customers2 = new ArrayList<Customer>();
        customers2.add(cust2);

        customers3 = new ArrayList<Customer>();
        customers3.add(cust3);


        Rides ride1 = new Rides(customers1, cust1.getGPSCoordinates(), new GPSCoordinate(51.7262433, 2.365247199999999, 84));
        Rides ride2 = new Rides(customers2, cust2.getGPSCoordinates(), new GPSCoordinate(52.7262433, 2.365247199999999, 84));
        Rides ride3 = new Rides(customers3, cust3.getGPSCoordinates(), new GPSCoordinate(53.7262433, 2.365247199999999, 84));

        ride1.setType(RideType.uberPool);
        ride2.setType(RideType.uberPool);
        ride3.setType(RideType.uberPool);

        rides = new ArrayList<Rides>();
        rides.add(ride1);
        rides.add(ride2);
        rides.add(ride3);

        Driver driver1 = new Driver("Smith1", "John1", DriverState.ON_DUTY);
        ArrayList<Driver> drivers1 = new ArrayList<Driver>();
        drivers1.add(driver1);
        Car myCar1 = new Standard(drivers1, driver1, new GPSCoordinate(60, 2.163283299999989, 158));

        Driver driver2 = new Driver("Smith2", "John2", DriverState.ON_DUTY);
        ArrayList<Driver> drivers2 = new ArrayList<Driver>();
        drivers2.add(driver2);
        Car myCar2 = new Standard(drivers2, driver2, new GPSCoordinate(50.7100841, 2.163283299999989, 158));

        ArrayList cars = new ArrayList<Car>();
        cars.add(myCar1);
        cars.add(myCar2);


        findCar = new FindCarRunnable(rides, cars);
    }

    @Test
    void sortRides() {
        findCar.sortRides();

        customers1.addAll(customers2);
        customers1.addAll(customers3);

        Rides ride = new Rides(customers1, customers1.get(0).getGPSCoordinates(), rides.get(2).getDestinationPoint());
        ride.setType(RideType.uberPool);

        ArrayList<GPSCoordinate> points = new ArrayList<GPSCoordinate>();
        points.add(customers2.get(0).getGPSCoordinates());
        points.add(customers3.get(0).getGPSCoordinates());
        points.add(rides.get(0).getDestinationPoint());
        points.add(rides.get(1).getDestinationPoint());

        ride.setPoints(points);

        assertEquals(findCar.getRide().getPoints(), ride.getPoints());
        assertEquals(findCar.getRide().getStartingPoint(), ride.getStartingPoint());
        assertEquals(findCar.getRide().getDestinationPoint(), ride.getDestinationPoint());

    }


}