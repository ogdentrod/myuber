package is1220.myUber.group8.part2;

import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.enums.TrafficType;
import is1220.myUber.group8.part2.humans.Driver;

public class BookEntry {
	private Rides ride;
	private TrafficType trafficType;
	private Driver driver;
	private double duration;
	private double distance;
	
	
	/**
	 * Constructor. Computes duration and distance of the ride on the fly
	 * @param ride see the Rides class
	 */
	public BookEntry(Rides ride) {
		this.ride = ride;
		this.trafficType = ride.getTrafficType();
		//compute distance
        double distance = 0;

        if (ride.getType() == RideType.uberPool) {
            GPSCoordinate previous = ride.getStartingPoint();
            for (GPSCoordinate point : ride.getPoints()) {
				distance += point.distance(previous);
				previous = point;
            }
            distance += previous.distance(ride.getDestinationPoint());
        } else {
            distance = ride.getDestinationPoint().distance(ride.getStartingPoint());
        }



		this.distance = distance;
		// duration depending on traffic
		if(this.trafficType == TrafficType.LOW_TRAFFIC) {
			double duration = distance/15; //in hours
			this.duration = duration;
		} else {
			if (this.trafficType == TrafficType.HEAVY_TRAFFIC ) {
				double duration = distance/3; //in hours
				this.duration = duration;
			} else {
				double duration = distance/7.5; //in hours
				this.duration = duration;
			}
		}
	}
	
	
	//getters and setters
	public Rides getRide() {
		return ride;
	}
	public void setRide(Rides ride) {
		this.ride = ride;
	}
	public double getDuration() {
		return duration;
	}
	public void setDuration(double duration) {
		this.duration = duration;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	
}
