package is1220.myUber.group8.part2;

import java.util.ArrayList;

;

/** 
 * Class used as a registry of every ride that occurred 
 * @author Benoit Sepe et Cécile Boniteau
 * @version 1.0
 */
public class BookOfRides {
	

	private ArrayList<BookEntry> book;
	
	/**
	 * Constructor
	 */
	public BookOfRides () {
		this.book = new ArrayList<BookEntry>();
	}
	
	/**
	 * This method adds a ride to the book, by creating a bookEntry object.
	 * @param ride
	 */
	public void addRideToBook (Rides ride ) {
		//make the book entry
		BookEntry entry = new BookEntry(ride);
		book.add(entry);
	}
	
	
	public ArrayList<BookEntry> getBook() {
		return book;
	}

	public void setBook(ArrayList<BookEntry> book) {
		this.book = book;
	}

}
	

