package is1220.myUber.group8.part2.utils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

/**
 * Allows to capture the outputs of the console
 *
 */
public class ConsoleOutputCapturer {
    private ByteArrayOutputStream baos;
    private PrintStream previous;
    private boolean capturing;
    
    /**
     * Starts the redirection of output
     */
    public void start() {
        if (capturing) {
            return;
        }
        capturing = true;
        //keeping in store the "console"
        previous = System.out;      
        baos = new ByteArrayOutputStream();
        OutputStream outputStreamCombiner = new OutputStreamCombiner(Arrays.asList(previous, baos)); 
        PrintStream custom = new PrintStream(outputStreamCombiner);
        //Redirection of output
        System.setOut(custom);
    }
    
    /**
     * Stops the redirection of output
     * @return
     */
    public String stop() {
        if (!capturing) {
            return "";
        }
        //putting back the console as the place to output
        System.setOut(previous);
        //keeping in store the output
        String capturedValue = baos.toString(); 
        //setting to default attributes
        baos = null;
        previous = null;
        capturing = false;

        return capturedValue;
    }
    
    /**
     * Nested private class to manage OutputStreams
     *
     */
    private static class OutputStreamCombiner extends OutputStream {
        private List<OutputStream> outputStreams;
        
        /**
         * Constructor
         * @param outputStreams
         */
        public OutputStreamCombiner(List<OutputStream> outputStreams) {
            this.outputStreams = outputStreams;
        }
        
        /**
         * 
         */
        public void write(int b) throws IOException {
            for (OutputStream os : outputStreams) {
                os.write(b);
            }
        }
        
        /**
         * 
         */
        public void flush() throws IOException {
            for (OutputStream os : outputStreams) {
                os.flush();
            }
        }
        /**
         * 
         */
        public void close() throws IOException {
            for (OutputStream os : outputStreams) {
                os.close();
            }
        }
    }
}