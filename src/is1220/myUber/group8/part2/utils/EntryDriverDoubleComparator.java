package is1220.myUber.group8.part2.utils;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

import is1220.myUber.group8.part2.humans.Driver;

public class EntryDriverDoubleComparator implements Comparator<Map.Entry<Driver, Double>>{

	@Override
	public int compare(Entry<Driver, Double> entry1, Entry<Driver, Double> entry2) {
		return entry1.getValue().compareTo(entry2.getValue());
	}
}
