package is1220.myUber.group8.part2.utils;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

import is1220.myUber.group8.part2.humans.Customer;

public class EntryCustomerIntegerComparator implements Comparator<Map.Entry<Customer, Integer>>{

	@Override
	public int compare(Entry<Customer, Integer> entry1, Entry<Customer, Integer> entry2) {
		return entry1.getValue().compareTo(entry2.getValue());
	}

}
