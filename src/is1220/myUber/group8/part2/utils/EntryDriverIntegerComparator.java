package is1220.myUber.group8.part2.utils;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

import is1220.myUber.group8.part2.humans.Driver;

public class EntryDriverIntegerComparator implements Comparator<Map.Entry<Driver, Integer>>{

	@Override
	public int compare(Entry<Driver, Integer> entry1, Entry<Driver, Integer> entry2) {
		return entry1.getValue().compareTo(entry2.getValue());
	}

}
