package is1220.myUber.group8.part2.utils;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

import is1220.myUber.group8.part2.humans.Customer;

public class EntryCustomerDoubleComparator implements Comparator<Map.Entry<Customer, Double>>{

	@Override
	public int compare(Entry<Customer, Double> entry1, Entry<Customer, Double> entry2) {
		return entry1.getValue().compareTo(entry2.getValue());
	}
}
