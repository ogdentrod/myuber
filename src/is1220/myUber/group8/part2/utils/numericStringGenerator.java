package is1220.myUber.group8.part2.utils;

import java.util.UUID;

public class numericStringGenerator {
	
	/**
	 * Generate a random string to use as card number
	 * @return
	 */
	public static String generateString() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replaceAll("[^0-9]", "");
    }

}
