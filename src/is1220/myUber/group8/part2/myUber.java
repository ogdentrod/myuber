package is1220.myUber.group8.part2;

import is1220.myUber.group8.part2.algorithms.findCar.FindCarRunnable;
import is1220.myUber.group8.part2.cars.Car;
import is1220.myUber.group8.part2.enums.RideType;
import is1220.myUber.group8.part2.enums.TrafficType;
import is1220.myUber.group8.part2.humans.Customer;
import is1220.myUber.group8.part2.humans.Driver;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.util.ArrayList;

public class myUber {

    ArrayList<Driver> drivers;
    ArrayList<Car> cars;
    ArrayList<Customer> customers;
    ArrayList<Rides> poolRequest;
    ZonedDateTime begin;
    ZonedDateTime end;

    ArrayList<FindCarRunnable> findCarRunnables;

    BookOfRides myBook;

    public myUber() {
        findCarRunnables = new ArrayList<FindCarRunnable>();
        poolRequest = new ArrayList<Rides>();
        cars = new ArrayList<Car>();
        customers = new ArrayList<Customer>();
        drivers = new ArrayList<Driver>();
        myBook = new BookOfRides();
        begin = ZonedDateTime.now();
    }

    public myUber(ArrayList<Car> cars, ArrayList<Driver> drivers, ArrayList<Customer> customers) {
        this();
        this.cars = cars;
        this.drivers = drivers;
        this.customers = customers;
        this.begin = ZonedDateTime.now();
    }

    public FindCarRunnable findCar(ArrayList<Rides> ride) {

        if (ride.size() == 0 || ride.get(0).getType() == null) {
            return null;
        }

        if (ride.get(0).getType() == RideType.uberPool && ride.size() < 2) {
            return null;
        }

        FindCarRunnable findCarRunnable = new FindCarRunnable(ride, this.cars);
        Thread findCarThread = new Thread(findCarRunnable);
        findCarThread.start();

        this.findCarRunnables.add(findCarRunnable);

        return findCarRunnable;

    }

    public FindCarRunnable findCar(Rides ride) {
        ArrayList<Rides> rides = new ArrayList<Rides>();
        rides.add(ride);
        return findCar(rides);
    }

    // Returns true if ride is completed and searching for a car, false if not
    public FindCarRunnable addPool(Rides ride, boolean go) {
        this.poolRequest.add(ride);

        if (this.poolRequest.size() == 3 || this.poolRequest.size() > 1 && go) {
            FindCarRunnable findCarRunnable = this.findCar(new ArrayList<Rides>(poolRequest));
            this.poolRequest.clear();
            return findCarRunnable;
        }
        return null;
    }
    
    /**
     * Finds the TrafficType for the current moment
     * @return TrafficType
     */
    public TrafficType findTrafficType() {
    	TrafficType trafficType;
    	//obtain the current time at the execution of myUber
    	ZonedDateTime dateTime = ZonedDateTime.now();
    	int currentTime = dateTime.get(ChronoField.CLOCK_HOUR_OF_DAY);

        return findTrafficType(currentTime);
    }
    
    /**
     * Find the Traffic Type for the specified hour
     * @param currentTime
     * @return
     */
    public TrafficType findTrafficType(Integer currentTime) {
    	TrafficType trafficType;
    	//draw a random number between 0 and 1
    	double drawnProbability = Math.random();
    	//disjunction depending of the current time
    	if (currentTime > 22 || currentTime <= 7) {
    		if(drawnProbability <= 0.95) {
    			trafficType = TrafficType.LOW_TRAFFIC;
    		} else { 
    			if (drawnProbability <= 0.99) {
    				trafficType = TrafficType.MEDIUM_TRAFFIC;
	    		} else {
	    			trafficType = TrafficType.HEAVY_TRAFFIC;
	    		}
    	}
    } else { 
    	if (currentTime > 7 || currentTime <= 11) {
			if(drawnProbability <= 0.05) {
				trafficType = TrafficType.LOW_TRAFFIC;
			} else { 
				if (drawnProbability <= 0.25) {
					trafficType = TrafficType.MEDIUM_TRAFFIC;
	    		} else {
	    			trafficType = TrafficType.HEAVY_TRAFFIC;
	    		}
			}
    } else { 
    	if (currentTime > 11 || currentTime <= 17) {
			if(drawnProbability <= 0.15) {
				trafficType = TrafficType.LOW_TRAFFIC;
			} else { 
				if (drawnProbability <= 0.85) {
					trafficType = TrafficType.MEDIUM_TRAFFIC;
	    		} else {
	    			trafficType = TrafficType.HEAVY_TRAFFIC;
	    		}
			}
	} else {
		if(drawnProbability <= 0.15) {
			trafficType = TrafficType.LOW_TRAFFIC;
		} else { 
			if (drawnProbability <= 0.85) {
				trafficType = TrafficType.MEDIUM_TRAFFIC;
    		} else {
    			trafficType = TrafficType.HEAVY_TRAFFIC;
    		}
		}
	}}}
    return trafficType;
    }


    public ArrayList<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(ArrayList<Driver> drivers) {
        this.drivers = drivers;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public ArrayList<FindCarRunnable> getFindCarRunnables() {
        return findCarRunnables;
    }

    public BookOfRides getMyBook() {
        return myBook;
    }
    
    
    public ZonedDateTime getBegin() {
		return begin;
	}

	public void setBegin(ZonedDateTime begin) {
		this.begin = begin;
	}

	public ZonedDateTime getEnd() {
		return end;
	}

	public void setEnd(ZonedDateTime end) {
		this.end = end;
	}

	public void addCustomer(Customer customer) {
    	this.customers.add(customer);
    }
    
    public void addCar(Car car) {
    	this.cars.add(car);
    }
    
    public void addDriver(Driver driver) {
    	this.drivers.add(driver);
    }
}
